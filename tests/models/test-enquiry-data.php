<?php
/**
 * Class TestCEFEnquiry_Data
 *
 * @package CEF
 */

/**
 * Test class for CEF_Enquiry_Data
 */
class TestCEF_Enquiry_Data extends WP_UnitTestCase {

	/**
	 * Remove the filter for temporary tables
	 *
	 * @return void
	 */
	public function setup() {
		parent::setup();
		remove_filter( 'query', array( $this, '_create_temporary_tables' ) );
		remove_filter( 'query', array( $this, '_drop_temporary_tables' ) );
	}

	/**
	 * Test set_data when all fields are set
	 *
	 * @return void
	 */
	public function test_set_data_all_fields() {
		$data = array(
			'first_name' => 'Carlos',
			'last_name'  => 'Alvarez',
			'email'      => 'carlosenam@hotmail.com',
			'subject'    => 'Testing',
			'message'    => 'This is testing',
		);

		$ed = new CEF_Enquiry_Data();
		$ed->set_data( $data );
		$ed_data = $ed->get_data();

		$this->assertArrayHasKey( 'first_name', $ed_data );
		$this->assertSame( $ed_data['first_name'], $data['first_name'] );
		$this->assertArrayHasKey( 'last_name', $ed_data );
		$this->assertSame( $ed_data['last_name'], $data['last_name'] );
		$this->assertArrayHasKey( 'email', $ed_data );
		$this->assertSame( $ed_data['email'], $data['email'] );
		$this->assertArrayHasKey( 'subject', $ed_data );
		$this->assertSame( $ed_data['subject'], $data['subject'] );
		$this->assertArrayHasKey( 'message', $ed_data );
		$this->assertSame( $ed_data['message'], $data['message'] );
	}

	/**
	 * Test set_data when some fields are set
	 *
	 * @return void
	 */
	public function test_set_data_some_fields() {
		$data = array(
			'first_name' => 'Carlos',
			'last_name'  => 'Alvarez',
		);

		$ed = new CEF_Enquiry_Data();
		$ed->set_data( $data );
		$ed_data = $ed->get_data();

		$this->assertArrayHasKey( 'first_name', $ed_data );
		$this->assertSame( $ed_data['first_name'], $data['first_name'] );
		$this->assertArrayHasKey( 'last_name', $ed_data );
		$this->assertSame( $ed_data['last_name'], $data['last_name'] );
		$this->assertArrayHasKey( 'email', $ed_data );
		$this->assertSame( '', $ed_data['email'] );
		$this->assertArrayHasKey( 'subject', $ed_data );
		$this->assertSame( '', $ed_data['subject'] );
		$this->assertArrayHasKey( 'message', $ed_data );
		$this->assertSame( '', $ed_data['message'] );
	}

	/**
	 * Test set_data when $data is not array
	 *
	 * @return void
	 */
	public function test_set_data_data_is_not_array() {
		$data = 1;

		$ed = new CEF_Enquiry_Data();
		try {
			$ed_data = $ed->set_data( $data );
		} catch ( Exception $e ) {
			$this->assertSame( $e->getMessage(), '$data is not an array' );
		}
	}

	/**
	 * Test save function
	 *
	 * @return void
	 */
	public function test_save() {

		CEF()->activate();

		$ed = new CEF_Enquiry_Data();
		$ed->set_data(
			array(
				'first_name' => 'Carlos',
				'last_name'  => 'Alvarez',
				'email'      => 'carlosenam@hotmail.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			)
		);

		$ed_id = $ed->save();

		$this->assertTrue( gettype( $ed_id ) === 'integer' );

		global $wpdb;

		$table_name = $wpdb->prefix . 'cef_form_data';

		$myed = $wpdb->get_row( "SELECT * FROM $table_name  WHERE id = $ed_id", ARRAY_A );

		$this->assertSame( $myed['first_name'], 'Carlos' );
		$this->assertSame( $myed['last_name'], 'Alvarez' );
		$this->assertSame( $myed['email'], 'carlosenam@hotmail.com' );
		$this->assertSame( $myed['subject'], 'Testing' );
		$this->assertSame( $myed['message'], 'This is testing' );
	}

	/**
	 * Store dummy data
	 *
	 * @return array dummy data
	 */
	public function dummy_data() {
		global $wpdb;

		$data = array(
			array(
				'first_name' => 'Carlos',
				'last_name'  => 'Alvarez',
				'email'      => 'carlosenam@hotmail.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Dennet',
				'last_name'  => 'Bort',
				'email'      => 'dennet@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Fabiola',
				'last_name'  => 'Cats',
				'email'      => 'fabiola@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Enrique',
				'last_name'  => 'Dart',
				'email'      => 'enrique@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Gary',
				'last_name'  => 'Emac',
				'email'      => 'gary@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Harold',
				'last_name'  => 'Ferret',
				'email'      => 'harold@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Irene',
				'last_name'  => 'Garot',
				'email'      => 'Irene@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Jake',
				'last_name'  => 'Hermes',
				'email'      => 'jake@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Katerine',
				'last_name'  => 'Ipsos',
				'email'      => 'katerine@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Larry',
				'last_name'  => 'Jefrey',
				'email'      => 'larry@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Mery',
				'last_name'  => 'Lako',
				'email'      => 'mery@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Nancy',
				'last_name'  => 'Mongomery',
				'email'      => 'nancy@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
		);

		$ed = new CEF_Enquiry_Data();

		foreach ( $data as $ed_data ) {
			$ed->set_data( $ed_data );
			$ed->save();
		}

		return $data;
	}

	/**
	 * Test the get_enquiry_data with default values
	 *
	 * @return void
	 */
	public function test_get_enquiry_data_default_values() {
		$data   = $this->dummy_data();
		$result = CEF_Enquiry_Data::get_enquiry_data();

		$is_correct = true;

		$this->assertCount( 10, $result );

		$data_count = count( $result );

		for ( $i = 0; $i < $data_count; $i++ ) {
			if ( $data[ $i ]['email'] !== $result[ $i ]['email'] ) {
				$is_correct = false;
				break;
			}
		}

		$this->assertTrue( $is_correct );
	}

	/**
	 * Test get_enquiry_data when page = 2
	 *
	 * @return void
	 */
	public function test_get_enquiry_data_page_2() {
		$data   = $this->dummy_data();
		$result = CEF_Enquiry_Data::get_enquiry_data( 2 );

		$is_correct = true;

		$this->assertCount( 2, $result );

		$data_count = count( $result );

		for ( $i = 0; $i < $data_count; $i++ ) {
			if ( $data[ $i + 10 ]['email'] !== $result[ $i ]['email'] ) {
				$is_correct = false;
				break;
			}
		}

		$this->assertTrue( $is_correct );
	}

	/**
	 * Test get_enquiry_data when message has to be trimmed
	 *
	 * @return void
	 */
	public function test_get_enquiry_data_message_has_to_be_trimmed() {
		global $wpdb;

		$data = array(
			'first_name' => 'Carlos',
			'last_name'  => 'Alvarez',
			'email'      => 'carlosenam@hotmail.com',
			'subject'    => 'Testing',
			'message'    => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.',
			'date'       => current_time( 'Y-m-d H:i:s' ),
		);

		$ed = new CEF_Enquiry_Data();
		$ed->set_data( $data );
		$ed->save();

		$result = CEF_Enquiry_Data::get_enquiry_data();

		$this->assertCount( 1, $result );

		$this->assertSame( $result[0]['message'], wp_trim_words( $data['message'], 10, '...' ) );
	}

	/**
	 * Test get_enquiry_data when message is not trimmed
	 *
	 * @return void
	 */
	public function test_get_enquiry_data_message_is_not_trimmed() {
		global $wpdb;

		$data = array(
			'first_name' => 'Carlos',
			'last_name'  => 'Alvarez',
			'email'      => 'carlosenam@hotmail.com',
			'subject'    => 'Testing',
			'message'    => 'Contrary to popular belief',
			'date'       => current_time( 'Y-m-d H:i:s' ),
		);

		$ed = new CEF_Enquiry_Data();
		$ed->set_data( $data );
		$ed->save();

		$result = CEF_Enquiry_Data::get_enquiry_data();

		$this->assertCount( 1, $result );

		$this->assertSame( $result[0]['message'], $data['message'] );
	}

	/**
	 * Test the get_number_of_pages functions when page = 2
	 *
	 * @return void
	 */
	public function test_get_number_of_pages_page_2() {
		$this->dummy_data();

		$pages = CEF_Enquiry_Data::get_number_of_pages();

		$this->assertSame( 2, $pages );
	}

	/**
	 * Test the get_number_of_pages functions when page = 1
	 *
	 * @return void
	 */
	public function test_get_number_of_pages_just_one_page() {
		$data = array(
			array(
				'first_name' => 'Carlos',
				'last_name'  => 'Alvarez',
				'email'      => 'carlosenam@hotmail.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Dennet',
				'last_name'  => 'Bort',
				'email'      => 'dennet@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
		);

		$ed = new CEF_Enquiry_Data();

		foreach ( $data as $ed_data ) {
			$ed->set_data( $ed_data );
			$ed->save();
		}

		$pages = CEF_Enquiry_Data::get_number_of_pages();

		$this->assertSame( 1, $pages );
	}

	/**
	 * Test the get function
	 *
	 * @return void
	 */
	public function test_get_function() {
		$ed = new CEF_Enquiry_Data(
			array(
				'first_name' => 'Carlos',
				'last_name'  => 'Alvarez',
				'email'      => 'carlosenam@hotmail.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			)
		);

		$ed_id = $ed->save();

		$this->assertSame( intval( CEF_Enquiry_Data::get( $ed_id )['id'] ), $ed_id );
	}

	/**
	 * Test the get function when entry doesn't exist
	 *
	 * @return void
	 */
	public function test_get_function_when_entry_doesnt_exist() {
		$ed = new CEF_Enquiry_Data(
			array(
				'first_name' => 'Carlos',
				'last_name'  => 'Alvarez',
				'email'      => 'carlosenam@hotmail.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			)
		);

		$ed_id = $ed->save();

		$this->assertCount( 0, CEF_Enquiry_Data::get( 0 ) );
	}


}
