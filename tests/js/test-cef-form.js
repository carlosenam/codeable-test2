// Tests are run using just one enquiry form
CEFFormId = $('.cef_enquiry_form').get(0).id;
CEFFormSelector = '.cef_enquiry_form#' + CEFFormId + ' ';

// CEFIsEmail Function

QUnit.module('CEFIsEmail function');

QUnit.test('Valid email', (assert) => {
  const email = 'carlosenam@hotmail.com';
  assert.ok(CEFIsEmail(email));
});

QUnit.test('Valid email with trailing spaces', (assert) => {
  const email = '  carlosenam@hotmail.com  ';
  assert.ok(CEFIsEmail(email));
});

QUnit.test('Invalid email with no domain', (assert) => {
  const email = 'carlosenam@';
  assert.notOk(CEFIsEmail(email));
});

QUnit.test('Invalid email with no @', (assert) => {
  const email = 'carlosenamhotmail.com';
  assert.notOk(CEFIsEmail(email));
});

QUnit.test('Invalid email with strange characters', (assert) => {
  const email = 'carlos///****!!!!enamhotmail.com';
  assert.notOk(CEFIsEmail(email));
});

QUnit.test('Empty string', (assert) => {
  const email = '';
  assert.notOk(CEFIsEmail(email));
});

QUnit.test('Invalid email with inner spaces', (assert) => {
  const email = 'carlos  @hotmail.  com';
  assert.notOk(CEFIsEmail(email));
});

// CEFValidateFields Function

QUnit.module('CEFValidateFields function', {
  after: () => {
    $('#cef_first_name').val('');
    $('#cef_last_name').val('');
    $('#cef_email').val('');
    $('#cef_subject').val('');
    $('#cef_message').val('');
  },
});

QUnit.test('All fields are valid', (assert) => {
  $('#cef_first_name').val('Carlos');
  $('#cef_last_name').val('Alvarez');
  $('#cef_email').val('carlosenam@hotmail.com');
  $('#cef_subject').val('Testing');
  $('#cef_message').val('Hi, this is a test');

  const result = CEFValidateFields();
  assert.equal(typeof result, 'object');
  assert.equal(typeof result.result, 'string');
  assert.equal(typeof result.fields, 'object');
  assert.equal(typeof result.fields.cef_first_name, 'string');
  assert.equal(typeof result.fields.cef_last_name, 'string');
  assert.equal(typeof result.fields.cef_email, 'string');
  assert.equal(typeof result.fields.cef_subject, 'string');
  assert.equal(typeof result.fields.cef_message, 'string');

  assert.equal(result.result, 'ok');
  assert.equal(result.fields.cef_first_name, $('#cef_first_name').val());
  assert.equal(result.fields.cef_last_name, $('#cef_last_name').val());
  assert.equal(result.fields.cef_email, $('#cef_email').val());
  assert.equal(result.fields.cef_subject, $('#cef_subject').val());
  assert.equal(result.fields.cef_message, $('#cef_message').val());
});

QUnit.test('Some fields are invalid', (assert) => {
  $('#cef_first_name').val('');
  $('#cef_last_name').val('Alvarez');
  $('#cef_email').val('carlose/**nam@hotmail.com');
  $('#cef_subject').val('Testing');
  $('#cef_message').val('');

  const result = CEFValidateFields();

  assert.equal(typeof result, 'object');
  assert.equal(typeof result.result, 'string');
  assert.equal(typeof result.fields, 'object');
  assert.equal(typeof result.fields.cef_first_name, 'string');
  assert.equal(typeof result.fields.cef_last_name, 'undefined');
  assert.equal(typeof result.fields.cef_email, 'string');
  assert.equal(typeof result.fields.cef_subject, 'undefined');
  assert.equal(typeof result.fields.cef_message, 'string');

  assert.equal(result.result, 'error');
  assert.equal(result.fields.cef_first_name, 'This field is required');
  assert.equal(result.fields.cef_email, 'Invalid Email');
  assert.equal(result.fields.cef_message, 'This field is required');
});

QUnit.test('All fields are invalid', (assert) => {
  $('#cef_first_name').val('');
  $('#cef_last_name').val('');
  $('#cef_email').val('carlose/**nam@hotmail.com');
  $('#cef_subject').val('');
  $('#cef_message').val('');

  const result = CEFValidateFields();

  assert.equal(typeof result, 'object');
  assert.equal(typeof result.result, 'string');
  assert.equal(typeof result.fields, 'object');
  assert.equal(typeof result.fields.cef_first_name, 'string');
  assert.equal(typeof result.fields.cef_last_name, 'string');
  assert.equal(typeof result.fields.cef_email, 'string');
  assert.equal(typeof result.fields.cef_subject, 'string');
  assert.equal(typeof result.fields.cef_message, 'string');

  assert.equal(result.result, 'error');
  assert.equal(result.fields.cef_first_name, 'This field is required');
  assert.equal(result.fields.cef_last_name, 'This field is required');
  assert.equal(result.fields.cef_email, 'Invalid Email');
  assert.equal(result.fields.cef_subject, 'This field is required');
  assert.equal(result.fields.cef_message, 'This field is required');
});

QUnit.test('Some fields are too long', (assert) => {
  $('#cef_first_name').val('asdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasd');
  $('#cef_last_name').val('asdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasd');
  $('#cef_email').val('carlosenam@hotmail.comasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasd');
  $('#cef_subject').val('asdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasd');
  $('#cef_message').val('asdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasdasdasdsadsadsadsadasd');

  const result = CEFValidateFields();

  assert.equal(typeof result, 'object');
  assert.equal(typeof result.result, 'string');
  assert.equal(typeof result.fields, 'object');
  assert.equal(typeof result.fields.cef_first_name, 'string');
  assert.equal(typeof result.fields.cef_last_name, 'string');
  assert.equal(typeof result.fields.cef_email, 'string');
  assert.equal(typeof result.fields.cef_subject, 'string');
  assert.equal(typeof result.fields.cef_message, 'string');

  assert.equal(result.result, 'error');
  assert.equal(result.fields.cef_first_name, 'Too long (100 chars max)');
  assert.equal(result.fields.cef_last_name, 'Too long (100 chars max)');
  assert.equal(result.fields.cef_email, 'Too long (100 chars max)');
  assert.equal(result.fields.cef_subject, 'Too long (100 chars max)');
  assert.equal(result.fields.cef_message, 'Too long (500 chars max)');
});

// CEFValidateField Function

QUnit.module('CEFValidateField function');

QUnit.test('Empty field', (assert) => {
  let result = CEFValidateField('cef_first_name', '');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'This field is required');
  result = CEFValidateField('cef_last_name', '');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'This field is required');
  result = CEFValidateField('cef_subject', '');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'This field is required');
  result = CEFValidateField('cef_message', '');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'This field is required');
  result = CEFValidateField('cef_email', '');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'This field is required');
});

QUnit.test('Invalid email', (assert) => {
  let result = CEFValidateField('cef_email', 'carlos///asdasd');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'error');
  assert.equal(result.message, 'Invalid Email');
});

QUnit.test('Valid Field', (assert) => {
  let result = CEFValidateField('cef_first_name', 'Carlos');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'ok');
  assert.equal(result.message, 'Carlos');
  result = CEFValidateField('cef_last_name', 'Alvarez');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'ok');
  assert.equal(result.message, 'Alvarez');
  result = CEFValidateField('cef_subject', 'Testing');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'ok');
  assert.equal(result.message, 'Testing');
  result = CEFValidateField('cef_message', 'This is a test');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'ok');
  assert.equal(result.message, 'This is a test');
  result = CEFValidateField('cef_email', 'carlosenam@hotmail.com');
  assert.equal(typeof result, 'object');
  assert.equal(result.result, 'ok');
  assert.equal(result.message, 'carlosenam@hotmail.com');
});

// CEFShowErrors Function

QUnit.module('CEFShowErrors function');

QUnit.test('All fields are invalid', (assert) => {
  const errorMessages = {
    cef_first_name: 'This field is required',
    cef_last_name: 'This field is required',
    cef_email: 'Invalid Email',
    cef_subject: 'This field is required',
    cef_message: 'This field is required',
  };

  CEFShowErrors(errorMessages);

  assert.ok($(CEFFormSelector + '#cef_first_name').next().hasClass('cef_error'));
  assert.ok($(CEFFormSelector + '#cef_last_name').next().hasClass('cef_error'));
  assert.ok($(CEFFormSelector + '#cef_email').next().hasClass('cef_error'));
  assert.ok($(CEFFormSelector + '#cef_subject').next().hasClass('cef_error'));
  assert.ok($(CEFFormSelector + '#cef_message').next().hasClass('cef_error'));

  assert.equal($(CEFFormSelector + '#cef_first_name').next().html(), 'This field is required');
  assert.equal($(CEFFormSelector + '#cef_last_name').next().html(), 'This field is required');
  assert.equal($(CEFFormSelector + '#cef_email').next().html(), 'Invalid Email');
  assert.equal($(CEFFormSelector + '#cef_subject').next().html(), 'This field is required');
  assert.equal($(CEFFormSelector + '#cef_message').next().html(), 'This field is required');
});

QUnit.test('Some fields are invalid', (assert) => {
  const errorMessages = {
    cef_first_name: 'This field is required',
    cef_email: 'Invalid Email',
    cef_message: 'This field is required',
  };

  CEFShowErrors(errorMessages);

  assert.ok($(CEFFormSelector + '#cef_first_name').next().hasClass('cef_error'));
  assert.notOk($(CEFFormSelector + '#cef_last_name').next().hasClass('cef_error'));
  assert.ok($(CEFFormSelector + '#cef_email').next().hasClass('cef_error'));
  assert.notOk($(CEFFormSelector + '#cef_subject').next().hasClass('cef_error'));
  assert.ok($(CEFFormSelector + '#cef_message').next().hasClass('cef_error'));

  assert.equal($(CEFFormSelector + '#cef_first_name').next().html(), 'This field is required');
  assert.notEqual($(CEFFormSelector + '#cef_last_name').next().html(), 'This field is required');
  assert.equal($(CEFFormSelector + '#cef_email').next().html(), 'Invalid Email');
  assert.notEqual($(CEFFormSelector + '#cef_subject').next().html(), 'This field is required');
  assert.equal($(CEFFormSelector + '#cef_message').next().html(), 'This field is required');
});

QUnit.test('Empty errorMessages', (assert) => {
  const errorMessages = {};
  CEFShowErrors(errorMessages);

  assert.notOk($(CEFFormSelector + '#cef_first_name').next().hasClass('cef_error'));
  assert.notOk($(CEFFormSelector + '#cef_last_name').next().hasClass('cef_error'));
  assert.notOk($(CEFFormSelector + '#cef_email').next().hasClass('cef_error'));
  assert.notOk($(CEFFormSelector + '#cef_subject').next().hasClass('cef_error'));
  assert.notOk($(CEFFormSelector + '#cef_message').next().hasClass('cef_error'));
});

// CEFOnSuccess Function

QUnit.module('CEFOnSucsess function');

QUnit.test('Form data is stored succesfully', (assert) => {
  const response = {
    result: 'ok',
  };

  CEFOnSuccess(response);
  assert.ok($(CEFFormSelector + '.cef_loader').hasClass('hidden'));
  assert.notOk($(CEFFormSelector + '.cef_result').hasClass('hidden'));
});

QUnit.test('Server returns error messages', (assert) => {
  const response = {
    result: 'error',
    fields: {
      cef_email: 'Invalid Email',
    },
  };

  CEFOnSuccess(response);

  assert.ok($(CEFFormSelector + '.cef_loader').hasClass('hidden'));
  assert.ok($(CEFFormSelector + '#cef_email').next().hasClass('cef_error'));
});

// Loader Module

QUnit.module('Loader');

QUnit.test('Show loader', (assert) => {
  CEFShowLoader();

  assert.notOk($(CEFFormSelector + '.cef_loader').hasClass('hidden'));
});

QUnit.test('Hide loader', (assert) => {
  CEFHideLoader();

  assert.ok($(CEFFormSelector + '.cef_loader').hasClass('hidden'));
});

// Show Results Module

QUnit.module('Show Results');

QUnit.test('Show succesful result', (assert) => {
  const resultMsg = 'Thank you for sending us your feedback';
  const msgClass = 'cef_success';

  CEFShowResult(resultMsg, msgClass);

  assert.notOk($(CEFFormSelector + '.cef_result').hasClass('hidden'));
  assert.equal($(CEFFormSelector + '.cef_result p').html(), 'Thank you for sending us your feedback');
  assert.ok($(CEFFormSelector + '.cef_result p').hasClass('cef_success'));
  assert.ok($(CEFFormSelector + '.cef_form_wrapper').hasClass('hidden'));
});

QUnit.test('Show system error message', (assert) => {
  const resultMsg = 'System error: Please try again later';
  const msgClass = 'cef_system_error';

  CEFShowResult(resultMsg, msgClass);

  assert.notOk($(CEFFormSelector + '.cef_result').hasClass('hidden'));
  assert.equal($(CEFFormSelector + '.cef_result p').html(), 'System error: Please try again later');
  assert.ok($(CEFFormSelector + '.cef_result p').hasClass('cef_system_error'));
  assert.ok($(CEFFormSelector + '.cef_form_wrapper').hasClass('hidden'));
});

QUnit.test('Hide successful result', (assert) => {
  CEFHideResult(CEFFormId);

  assert.ok($(CEFFormSelector + '.cef_result').hasClass('hidden'));
  assert.notOk($(CEFFormSelector + '.cef_form_wrapper').hasClass('hidden'));
});

// CEFResetForm Function

QUnit.module('resetForm function');

QUnit.test('Form have some valid inputs', (assert) => {
  $('#cef_first_name').val('');
  $('#cef_last_name').val('Alvarez');
  $('#cef_email').val('carlose/**nam@hotmail.com');
  $('#cef_subject').val('Testing');
  $('#cef_message').val('');

  CEFResetForm();

  assert.equal('', $('#cef_first_name').val());
  assert.equal('', $('#cef_last_name').val());
  assert.equal('', $('#cef_email').val());
  assert.equal('', $('#cef_subject').val());
  assert.equal('', $('#cef_message').val());
  assert.equal(0, $('.cef_error').length);
});

QUnit.test('Form have all valid inputs', (assert) => {
  $('#cef_first_name').val('Carlos');
  $('#cef_last_name').val('Alvarez');
  $('#cef_email').val('carlosenam@hotmail.com');
  $('#cef_subject').val('Testing');
  $('#cef_message').val('This is testing');

  CEFResetForm();

  assert.equal('', $('#cef_first_name').val());
  assert.equal('', $('#cef_last_name').val());
  assert.equal('', $('#cef_email').val());
  assert.equal('', $('#cef_subject').val());
  assert.equal('', $('#cef_message').val());
  assert.equal(0, $('.cef_error').length);
});

// CEFAttachEventHandlers Function

QUnit.module('CEFAttachEventHandlers function');

QUnit.test('Submit handler is attached to the form', (assert) => {
  CEFAttachEventHandlers();
  assert.equal(typeof $._data( $('.cef_form')[0], 'events' ).submit, 'object');
});

QUnit.test('Submit handler is attached to the form', (assert) => {
  CEFAttachEventHandlers();
  assert.equal(typeof $._data( $('.cef_back')[0], 'events' ).click, 'object');
});
