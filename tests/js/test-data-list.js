CEFActiveDataListId = $('.cef_data_list').get(0).id;
CEFActiveDataListSelector = '.cef_data_list#' + CEFActiveDataListId + ' ';

// CEFFillList Function

QUnit.module('CEFFillList function', {
  before: () => {
    $('.cef_data_list .cef_data_table tbody tr').remove();
  },
  afterEach: () => {
    $('.cef_data_list .cef_data_table tbody tr').remove();
  },
});

QUnit.test('1 entry, page = 1', (assert) => {
  const response = [
    {
      id: '6',
      email: 'carlosenam@hotmail.com',
      subject: 'asdasdasd',
      message: 'adasdasdsa',
    },
  ];

  CEFFillList(response, 1, 1, CEFActiveDataListId);
  assert.equal(1, $('.cef_data_list .cef_data_table tbody tr').length);
  assert.equal('1', $('.cef_data_list .cef_data_table tbody tr td:nth-child(1)').html());
  assert.equal(response[0].email, $('.cef_data_list .cef_data_table tbody tr td:nth-child(2)').html());
  assert.equal(response[0].subject, $('.cef_data_list .cef_data_table tbody tr td:nth-child(3)').html());
  assert.equal(response[0].message, $('.cef_data_list .cef_data_table tbody tr td:nth-child(4)').html());
});

QUnit.test('1 entry, page = 2', (assert) => {
  const response = [
    {
      id: '6',
      email: 'carlosenam@hotmail.com',
      subject: 'asdasdasd',
      message: 'adasdasdsa',
    },
  ];

  CEFFillList(response, 2, 2, CEFActiveDataListId);
  assert.equal(1, $('.cef_data_list .cef_data_table tbody tr').length);
  assert.equal('11', $('.cef_data_list .cef_data_table tbody tr td:nth-child(1)').html());
  assert.equal(response[0].email, $('.cef_data_list .cef_data_table tbody tr td:nth-child(2)').html());
  assert.equal(response[0].subject, $('.cef_data_list .cef_data_table tbody tr td:nth-child(3)').html());
  assert.equal(response[0].message, $('.cef_data_list .cef_data_table tbody tr td:nth-child(4)').html());
});

QUnit.test('2 entries', (assert) => {
  const response = [
    {
      id: '6',
      email: 'carlosenam@hotmail.com',
      subject: 'asdasdasd',
      message: 'adasdasdsa',
    },
    {
      id: '7',
      email: 'carlos@hotmail.com',
      subject: 'qweqdwd',
      message: 'adas1wdqasdadasdsa',
    },
  ];

  CEFFillList(response, 1, 1, CEFActiveDataListId);
  assert.equal(2, $('.cef_data_list .cef_data_table tbody tr').length);
  assert.equal('1', $('.cef_data_list .cef_data_table tbody tr:nth-child(1) td:nth-child(1)').html());
  assert.equal(response[0].email, $('.cef_data_list .cef_data_table tbody tr:nth-child(1) td:nth-child(2)').html());
  assert.equal(response[0].subject, $('.cef_data_list .cef_data_table tbody tr:nth-child(1) td:nth-child(3)').html());
  assert.equal(response[0].message, $('.cef_data_list .cef_data_table tbody tr:nth-child(1) td:nth-child(4)').html());

  assert.equal('2', $('.cef_data_list .cef_data_table tbody tr:nth-child(2) td:nth-child(1)').html());
  assert.equal(response[1].email, $('.cef_data_list .cef_data_table tbody tr:nth-child(2) td:nth-child(2)').html());
  assert.equal(response[1].subject, $('.cef_data_list .cef_data_table tbody tr:nth-child(2) td:nth-child(3)').html());
  assert.equal(response[1].message, $('.cef_data_list .cef_data_table tbody tr:nth-child(2) td:nth-child(4)').html());
  assert.equal(1, $('.cef_data_list .cef_pages_selector option').length);
  assert.equal('1', $('.cef_data_list .cef_pages_selector').val());
});

// CEFToggleOrder Function

QUnit.module('CEFToggleOrder function', {
  beforeEach: () => {
    CEFFiltersValues[CEFActiveDataListId] = {
      page: 1,
      orderby: 'date',
      order: 'DESC',
    };
  },
});

QUnit.test('initial state with no order', (assert) => {
  const sortEl = $('.cef_sort:first');
  sortEl.removeAttr('order');
  const orderby = sortEl.attr('orderby');
  CEFToggleOrder(sortEl[0], CEFActiveDataListId);
  assert.equal(typeof CEFFiltersValues[CEFActiveDataListId], 'object');
  assert.equal(CEFFiltersValues[CEFActiveDataListId].order, 'ASC');
  assert.equal(CEFFiltersValues[CEFActiveDataListId].orderby, orderby);
});

QUnit.test('initial state with order = ASC', (assert) => {
  const sortEl = $('.cef_sort:first');
  sortEl.attr('order', 'ASC');
  const orderby = sortEl.attr('orderby');
  CEFToggleOrder(sortEl[0], CEFActiveDataListId);
  assert.equal(typeof CEFFiltersValues[CEFActiveDataListId], 'object');
  assert.equal(CEFFiltersValues[CEFActiveDataListId].order, 'DESC');
  assert.equal(CEFFiltersValues[CEFActiveDataListId].orderby, orderby);
});

QUnit.test('initial state with order = DESC', (assert) => {
  const sortEl = $('.cef_sort:first');
  sortEl.attr('order', 'DESC');
  const orderby = sortEl.attr('orderby');
  CEFToggleOrder(sortEl[0], CEFActiveDataListId);
  assert.equal(typeof CEFFiltersValues[CEFActiveDataListId], 'object');
  assert.equal(CEFFiltersValues[CEFActiveDataListId].order, 'DESC');
  assert.equal(CEFFiltersValues[CEFActiveDataListId].orderby, 'date');
});

// Loader Module

QUnit.module('Loader Module');

QUnit.test('Show Loader', (assert) => {
  $('.cef_loader').addClass('hidden');
  CEFShowLoaderDataList(CEFActiveDataListId);
  assert.notOk($('.cef_loader').hasClass('hidden'));
});

QUnit.test('Hide Loader', (assert) => {
  $('.cef_loader').removeClass('hidden');
  CEFHideLoaderDataList(CEFActiveDataListId);
  assert.ok($('.cef_loader').hasClass('hidden'));
});

// CEFGetDataListSelector Function

QUnit.module('CEFGetDataListSelector function');

QUnit.test('return selector correctly', (assert) => {
  assert.equal(CEFGetDataListSelector(CEFActiveDataListId), '.cef_data_list#' + CEFActiveDataListId + ' ');
});

// CEFFillDetailedInfo Function

QUnit.module('CEFFillDetailedInfo function', {
  after: () => {
    $('.cef_date').html('');
    $('.cef_first_name').html('');
    $('.cef_last_name').html('');
    $('.cef_email').html('');
    $('.cef_subject').html('');
    $('.cef_message').html('');
  },
});

QUnit.test('fill the detailed info section', (assert) => {

  const entryData = {
    first_name: 'Carlos',
    last_name: 'Alvarez',
    email: 'carlosenam@hotmail.com',
    subject: 'Testing',
    message: 'Esto es un mensaje',
    date: '2019-09-20 12:30:00',
  }

  CEFFillDetailedInfo(entryData, CEFActiveDataListId);

  assert.equal(entryData.first_name, $('.cef_first_name').html());
  assert.equal(entryData.last_name, $('.cef_last_name').html());
  assert.equal(entryData.email, $('.cef_email').html());
  assert.equal(entryData.subject, $('.cef_subject').html());
  assert.equal(entryData.message, $('.cef_message').html());
  assert.equal(entryData.date, $('.cef_date').html());
});
