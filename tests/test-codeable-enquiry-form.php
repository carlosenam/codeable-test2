<?php
/**
 * Class TestCEF_MainClass
 *
 * @package CEF
 */

/**
 * Test class for CEF_MainClass
 */
class TestCEF_MainClass extends WP_UnitTestCase {

	/**
	 * Remove the filter for temporary tables
	 *
	 * @return void
	 */
	public function setup() {
		parent::setup();
		remove_filter( 'query', array( $this, '_create_temporary_tables' ) );
		remove_filter( 'query', array( $this, '_drop_temporary_tables' ) );
	}

	/**
	 * Delete the cef_form_data table from test database
	 *
	 * @return void
	 */
	public function tearDown() {
		parent::tearDown();

		global $wpdb;
		$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}cef_form_data" );
	}

	/**
	 * Test if global variables are defined and have the correct values
	 *
	 * @return void
	 */
	public function test_global_variables_are_defined_and_have_the_correct_values() {
		$this->assertTrue( defined( 'CEF_VERSION' ) );
		$this->assertSame( CEF_VERSION, '1.0.0' );
		$this->assertTrue( defined( 'CEF_PATH' ) );
		$this->assertRegExp( '/wp-content\/plugins\/codeable-enquiry-form\/$/', CEF_PATH );
		$this->assertTrue( defined( 'CEF_INC_PATH' ) );
		$this->assertSame( CEF_INC_PATH, CEF_PATH . 'includes/' );
		$this->assertTrue( defined( 'CEF_URL' ) );
		$this->assertSame( CEF_URL, plugin_dir_url( CEF_PATH . 'codeable-enquiry-form.php' ) );
		$this->assertTrue( defined( 'CEF_ASSETS_URL' ) );
		$this->assertSame( CEF_ASSETS_URL, CEF_URL . 'assets/' );
	}


	/**
	 * Test if the shortcodes are added
	 *
	 * @return void
	 */
	public function test_shortcodes_are_added() {
		global $shortcode_tags;

		CEF();

		$cef_enquiry_form = 'cef_form';
		$cef_data_list    = 'cef_data_list';

		$this->assertTrue( has_action( 'init', array( CEF(), 'add_shortcodes' ) ) ? true : false );
		$this->assertArrayHasKey( $cef_enquiry_form, $shortcode_tags );
		$this->assertInstanceOf( 'CEF_Form_Shortcode', $shortcode_tags[ $cef_enquiry_form ][0] );
		$this->assertSame( 'html_output', $shortcode_tags[ $cef_enquiry_form ][1] );

		$this->assertArrayHasKey( $cef_data_list, $shortcode_tags );
		$this->assertInstanceOf( 'CEF_DataList_Shortcode', $shortcode_tags[ $cef_data_list ][0] );
		$this->assertSame( 'html_output', $shortcode_tags[ $cef_data_list ][1] );
	}

	/**
	 * Test if AJAX callbacks are added when no admin
	 *
	 * @return void
	 */
	public function test_ajax_callbacks_are_added_when_no_admin() {
		global $wp_filter;
		CEF();
		$process_data_callback = array_shift( $wp_filter['wp_ajax_cef_process_form_data']->callbacks[10] )['function'];
		$this->assertInstanceOf( 'CEF_Form_Shortcode', $process_data_callback[0] );
		$this->assertSame( $process_data_callback[1], 'process_form_data' );

		$process_data_callback_no_priv = array_shift( $wp_filter['wp_ajax_nopriv_cef_process_form_data']->callbacks[10] )['function'];
		$this->assertInstanceOf( 'CEF_Form_Shortcode', $process_data_callback_no_priv[0] );
		$this->assertSame( $process_data_callback_no_priv[1], 'process_form_data' );

		$this->assertFalse( isset( $wp_filter['wp_ajax_cef_get_enquiry_data'] ) );
	}

	/**
	 * Test if AJAX callbacks are added when admin
	 *
	 * @return void
	 */
	/*public function test_ajax_callbacks_are_added_when_admin() {
		global $wp_filter;
		wp_set_current_user( 1 );
		CEF();
		$process_data_callback = array_shift( $wp_filter['wp_ajax_cef_get_enquiry_data']->callbacks[10] )['function'];
		$this->assertInstanceOf( 'CEF_DataList_Shortcode', $process_data_callback[0] );
		$this->assertSame( $process_data_callback[1], 'get_enquiry_data' );
		$process_data_callback = array_shift( $wp_filter['wp_ajax_cef_get_enquiry_data_entry']->callbacks[10] )['function'];
		$this->assertInstanceOf( 'CEF_DataList_Shortcode', $process_data_callback[0] );
		$this->assertSame( $process_data_callback[1], 'get_enquiry_data_entry' );
	}*/

	/**
	 * Test if the plugin activation hook is activated
	 *
	 * @return void
	 */
	public function test_activation_hook_is_added() {
		$plugin_file_path = CEF_PATH . 'codeable-enquiry-form.php';
		$this->assertTrue( has_action( 'activate_' . plugin_basename( $plugin_file_path ), array( CEF(), 'activate' ) ) ? true : false );
	}

	/**
	 * Test if the 'cef_form data' table is created in the database
	 *
	 * @return void
	 */
	public function test_database_table_is_created() {
		global $wpdb;

		CEF()->activate();
		$table_name = $wpdb->prefix . 'cef_form_data';
		$this->assertSame( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ), $table_name );
	}
}
