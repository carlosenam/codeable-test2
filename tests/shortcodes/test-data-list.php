<?php
/**
 * Class TestCEF_Form_Shortcode
 *
 * @package CEF
 */

/**
 * Test Class for the CEF_DataList_Shortcode class
 */
class TestCEF_DataList_Shortcode extends WP_UnitTestCase {

	/**
	 * Test if the scripts and styles are enqueued for [cef_data_list]
	 *
	 * @return void
	 */
	public function test_scripts_and_styles_are_enqueued() {

		$cef_datalist_shortcode = new CEF_DataList_Shortcode();

		$cef_datalist_shortcode->enqueue_scripts_and_styles();

		$scripts_object = array(
			array(
				'id'   => 'cef-data-list',
				'url'  => CEF_ASSETS_URL . 'js/cef-data-list.js',
				'type' => 'script',
			),
			array(
				'id'   => 'cef-data-list-styles',
				'url'  => CEF_ASSETS_URL . 'css/cef-data-list.css',
				'type' => 'style',
			),
		);

		$result = true;

		foreach ( $scripts_object as $script_object ) {
			$id   = $script_object['id'];
			$url  = $script_object['url'];
			$type = $script_object['type'];
			if ( ! $this->script_is_enqueued( $id, $url, $type ) ) {
				$result = false;
				break;
			}
		}

		$this->assertTrue( $result );

		// Test localized scripts.
		global $wp_scripts;
		$data               = $wp_scripts->get_data( 'cef-data-list', 'data' );
		$data               = substr( $data, strpos( $data, '{' ) - 1, -1 );
		$cef_form_vars_data = json_decode( $data, true );
		$this->assertCount( 4, $cef_form_vars_data );
		$this->assertarrayHasKey( 'ajaxUrl', $cef_form_vars_data );
		$this->assertarrayHasKey( 'nonceFilter', $cef_form_vars_data );
		$this->assertarrayHasKey( 'nonceGetEntry', $cef_form_vars_data );
		$this->assertarrayHasKey( 'systemErrorText', $cef_form_vars_data );
	}

	/**
	 * Return true if a script/style is enqueued
	 *
	 * @param string $script_id id of the enqueued script/style.
	 * @param string $script_url url of the enqueued script/style.
	 * @param string $script_type it can be 'script' or 'style'.
	 * @return boolean
	 */
	public function script_is_enqueued( $script_id, $script_url, $script_type = 'script' ) {
		$script_array = [];
		global $wp_styles, $wp_scripts;
		$script_object;
		switch ( $script_type ) {
			case 'script':
				$script_object = $wp_scripts;
				break;
			case 'style':
				$script_object = $wp_styles;
				break;
		}

		foreach ( $script_object->queue as $script ) {
			if ( $script_object->registered[ $script ]->src === $script_url
				&& $script_id === $script ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Test the validate_params when all params are valid
	 *
	 * @return void
	 */
	public function test_validate_params_all_valid_params() {
		$cef_datalist_shortcode = new CEF_DataList_Shortcode();

		$post_data = array(
			'page'    => '1',
			'orderby' => 'email',
			'order'   => 'ASC',
		);

		$result = $cef_datalist_shortcode->validate_params( $post_data );

		$this->assertSame( 'array', gettype( $result ) );
		$this->assertArrayHasKey( 'page', $result );
		$this->assertArrayHasKey( 'orderby', $result );
		$this->assertArrayHasKey( 'order', $result );
	}

	/**
	 * Test the validate_params when page is invalid
	 *
	 * @return void
	 */
	public function test_validate_params_page_is_invalid() {
		$cef_datalist_shortcode = new CEF_DataList_Shortcode();

		$post_data = array(
			'page'    => 'asd',
			'orderby' => '12asda2',
			'order'   => 'ASC123',
		);

		$result = $cef_datalist_shortcode->validate_params( $post_data );

		$this->assertSame( 'string', gettype( $result ) );
		$this->assertSame( 'This param is not valid: page', $result );
	}

	/**
	 * Test the validate_params when orderby is invalid
	 *
	 * @return void
	 */
	public function test_validate_params_orderby_is_invalid() {
		$cef_datalist_shortcode = new CEF_DataList_Shortcode();

		$post_data = array(
			'page'    => '1',
			'orderby' => '12asda2',
			'order'   => 'ASC123',
		);

		$result = $cef_datalist_shortcode->validate_params( $post_data );

		$this->assertSame( 'string', gettype( $result ) );
		$this->assertSame( 'This param is not valid: orderby', $result );
	}

	/**
	 * Test the validate_params when order is invalid
	 *
	 * @return void
	 */
	public function test_validate_params_order_is_invalid() {
		$cef_datalist_shortcode = new CEF_DataList_Shortcode();

		$post_data = array(
			'page'    => '1',
			'orderby' => 'email',
			'order'   => 'ASC123',
		);

		$result = $cef_datalist_shortcode->validate_params( $post_data );

		$this->assertSame( 'string', gettype( $result ) );
		$this->assertSame( 'This param is not valid: order', $result );
	}

	/**
	 * Test the validate_params when orderby is invalid
	 *
	 * @return void
	 */
	public function test_validate_params_some_fields_are_undefined() {
		$cef_datalist_shortcode = new CEF_DataList_Shortcode();

		$post_data = array(
			'page' => '1',
		);

		$result = $cef_datalist_shortcode->validate_params( $post_data );

		$this->assertSame( 'array', gettype( $result ) );
		$this->assertArrayHasKey( 'page', $result );
		$this->assertArrayHasKey( 'orderby', $result );
		$this->assertArrayHasKey( 'order', $result );
		$this->assertSame( '1', $result['page'] );
		$this->assertSame( 'date', $result['orderby'] );
		$this->assertSame( 'DESC', $result['order'] );
	}

	/**
	 * Test the validate_params when id is invalid
	 *
	 * @return void
	 */
	public function test_validate_params_id_is_invalid() {
		$cef_datalist_shortcode = new CEF_DataList_Shortcode();

		$post_data = array();

		$result = $cef_datalist_shortcode->validate_params( $post_data, 'cef-get-entry' );

		$this->assertSame( 'string', gettype( $result ) );
		$this->assertSame( 'This param is not valid: id', $result );
	}

	/**
	 * Test the validate_params when id is valid
	 *
	 * @return void
	 */
	public function test_validate_params_id_is_valid() {
		$cef_datalist_shortcode = new CEF_DataList_Shortcode();

		$post_data = array(
			'id' => '1',
		);

		$result = $cef_datalist_shortcode->validate_params( $post_data, 'cef-get-entry' );

		$this->assertSame( 'array', gettype( $result ) );
		$this->assertArrayHasKey( 'id', $result );
	}

}
