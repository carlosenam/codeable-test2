<?php
/**
 * Class TestCEF_Form_Shortcode
 *
 * @package CEF
 */

/**
 * Test Class for the CEF_Form_Shortcode class
 */
class TestCEF_Form_Shortcode extends WP_UnitTestCase {

	/**
	 * Test if the scripts and styles are enqueued for [cef_form]
	 *
	 * @return void
	 */
	public function test_scripts_and_styles_are_enqueued() {

		$cef_form_shortcode = new CEF_Form_Shortcode();

		$cef_form_shortcode->enqueue_scripts_and_styles();

		$scripts_object = array(
			array(
				'id'   => 'cef-form',
				'url'  => CEF_ASSETS_URL . 'js/cef-form.js',
				'type' => 'script',
			),
			array(
				'id'   => 'cef-form-styles',
				'url'  => CEF_ASSETS_URL . 'css/cef-form.css',
				'type' => 'style',
			),
		);

		$result = true;

		foreach ( $scripts_object as $script_object ) {
			$id   = $script_object['id'];
			$url  = $script_object['url'];
			$type = $script_object['type'];
			if ( ! $this->script_is_enqueued( $id, $url, $type ) ) {
				$result = false;
				break;
			}
		}

		$this->assertTrue( $result );

		// Test localized scripts.
		global $wp_scripts;
		$data               = $wp_scripts->get_data( 'cef-form', 'data' );
		$data               = substr( $data, strpos( $data, '{' ) - 1, -1 );
		$cef_form_vars_data = json_decode( $data, true );
		$this->assertCount( 8, $cef_form_vars_data );
		$this->assertarrayHasKey( 'ajaxUrl', $cef_form_vars_data );
		$this->assertarrayHasKey( 'nonce', $cef_form_vars_data );
		$this->assertarrayHasKey( 'successText', $cef_form_vars_data );
		$this->assertarrayHasKey( 'systemErrorText', $cef_form_vars_data );
		$this->assertarrayHasKey( 'requiredFieldText', $cef_form_vars_data );
		$this->assertarrayHasKey( 'tooLong500Text', $cef_form_vars_data );
		$this->assertarrayHasKey( 'tooLong100Text', $cef_form_vars_data );
		$this->assertarrayHasKey( 'invalidEmailText', $cef_form_vars_data );
	}

	/**
	 * Test the validate_form_data function when all field values are valid
	 *
	 * @return void
	 */
	public function test_validate_form_data_all_fields_are_valid() {
		$cef_form_shortcode = new CEF_Form_Shortcode();

		$post_data = array(
			'cef_first_name' => 'Carlos',
			'cef_last_name'  => 'Alvarez',
			'cef_email'      => 'carlosenam@hotmail.com',
			'cef_subject'    => 'Testing',
			'cef_message'    => 'This is testing',
		);

		$validation_result = $cef_form_shortcode->validate_form_data( $post_data );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'fields', $validation_result );
		$this->assertArrayHasKey( 'first_name', $validation_result['fields'] );
		$this->assertArrayHasKey( 'last_name', $validation_result['fields'] );
		$this->assertArrayHasKey( 'email', $validation_result['fields'] );
		$this->assertArrayHasKey( 'subject', $validation_result['fields'] );
		$this->assertArrayHasKey( 'message', $validation_result['fields'] );
		$this->assertSame( $post_data['cef_first_name'], $validation_result['fields']['first_name'] );
		$this->assertSame( $post_data['cef_last_name'], $validation_result['fields']['last_name'] );
		$this->assertSame( $post_data['cef_email'], $validation_result['fields']['email'] );
		$this->assertSame( $post_data['cef_subject'], $validation_result['fields']['subject'] );
		$this->assertSame( $post_data['cef_message'], $validation_result['fields']['message'] );
	}

	/**
	 * Test the validate_form_data function some field values are invalid
	 *
	 * @return void
	 */
	public function test_validate_form_data_some_fields_are_invalid() {
		$cef_form_shortcode = new CEF_Form_Shortcode();

		$post_data = array(
			'cef_first_name' => 'Carlos',
			'cef_last_name'  => '',
			'cef_email'      => 'carlosenam@hotmail.  com',
			'cef_subject'    => '',
			'cef_message'    => 'This is testing',
		);

		$validation_result = $cef_form_shortcode->validate_form_data( $post_data );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'fields', $validation_result );
		$this->assertFalse( isset( $validation_result['fields']['first_name'] ) );
		$this->assertArrayHasKey( 'cef_last_name', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cef_email', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cef_subject', $validation_result['fields'] );
		$this->assertFalse( isset( $validation_result['fields']['message'] ) );
		$this->assertSame( 'This field is required', $validation_result['fields']['cef_last_name'] );
		$this->assertSame( 'Invalid Email', $validation_result['fields']['cef_email'] );
		$this->assertSame( 'This field is required', $validation_result['fields']['cef_subject'] );
	}

	/**
	 * Test the validate_form_data function some field values are undefined
	 *
	 * @return void
	 */
	public function test_validate_form_data_some_fields_are_undefined() {
		$cef_form_shortcode = new CEF_Form_Shortcode();

		$post_data = array(
			'cef_first_name' => 'Carlos',
			'cef_message'    => 'This is testing',
		);

		$validation_result = $cef_form_shortcode->validate_form_data( $post_data );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'fields', $validation_result );
		$this->assertFalse( isset( $validation_result['fields']['first_name'] ) );
		$this->assertArrayHasKey( 'cef_last_name', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cef_email', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cef_subject', $validation_result['fields'] );
		$this->assertFalse( isset( $validation_result['fields']['message'] ) );
		$this->assertSame( 'This field is required', $validation_result['fields']['cef_last_name'] );
		$this->assertSame( 'This field is required', $validation_result['fields']['cef_email'] );
		$this->assertSame( 'This field is required', $validation_result['fields']['cef_subject'] );
	}

	/**
	 * Test the validate_form_data function all field values are undefined
	 *
	 * @return void
	 */
	public function test_validate_form_data_all_fields_are_undefined() {
		$cef_form_shortcode = new CEF_Form_Shortcode();

		$post_data = array();

		$validation_result = $cef_form_shortcode->validate_form_data( $post_data );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'fields', $validation_result );
		$this->assertFalse( isset( $validation_result['fields']['first_name'] ) );
		$this->assertArrayHasKey( 'cef_last_name', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cef_email', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cef_subject', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cef_message', $validation_result['fields'] );
		$this->assertArrayHasKey( 'cef_first_name', $validation_result['fields'] );
		$this->assertFalse( isset( $validation_result['fields']['message'] ) );
		$this->assertSame( 'This field is required', $validation_result['fields']['cef_last_name'] );
		$this->assertSame( 'This field is required', $validation_result['fields']['cef_email'] );
		$this->assertSame( 'This field is required', $validation_result['fields']['cef_subject'] );
		$this->assertSame( 'This field is required', $validation_result['fields']['cef_message'] );
		$this->assertSame( 'This field is required', $validation_result['fields']['cef_first_name'] );
	}

	/**
	 * Test the validate_single_form_field function when the field is empty
	 *
	 * @return void
	 */
	public function test_validate_single_form_field_when_empty_field() {
		$cef_form_shortcode = new CEF_Form_Shortcode();

		$id    = 'first_name';
		$value = '';

		$validation_result = $cef_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'This field is required', $validation_result['message'] );

		$id = 'last_name';

		$validation_result = $cef_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'This field is required', $validation_result['message'] );

		$id = 'email';

		$validation_result = $cef_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'This field is required', $validation_result['message'] );

		$id = 'subject';

		$validation_result = $cef_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'This field is required', $validation_result['message'] );

		$id = 'message';

		$validation_result = $cef_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'This field is required', $validation_result['message'] );
	}

	/**
	 * Test the validate_single_form_field function when an email is invalid
	 *
	 * @return void
	 */
	public function test_validate_single_form_field_when_invalid_email() {
		$cef_form_shortcode = new CEF_Form_Shortcode();

		$id    = 'email';
		$value = 'carlos@_|@@';

		$validation_result = $cef_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'error', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( 'Invalid Email', $validation_result['message'] );
	}

	/**
	 * Test the validate_single_form_field function when the field value is valid
	 *
	 * @return void
	 */
	public function test_validate_single_form_field_when_valid_field_value() {
		$cef_form_shortcode = new CEF_Form_Shortcode();

		$id    = 'Carlos';
		$value = 'first_name';

		$validation_result = $cef_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( $value, $validation_result['message'] );

		$id    = 'last_name';
		$value = 'Alvarez';

		$validation_result = $cef_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( $value, $validation_result['message'] );

		$id    = 'email';
		$value = 'carlos@hotmail.com';

		$validation_result = $cef_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( $value, $validation_result['message'] );

		$id    = 'subject';
		$value = 'Testing';

		$validation_result = $cef_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( $value, $validation_result['message'] );

		$id    = 'message';
		$value = 'This is a test';

		$validation_result = $cef_form_shortcode->validate_single_form_field( $id, $value );

		$this->assertArrayHasKey( 'result', $validation_result );
		$this->assertSame( 'ok', $validation_result['result'] );
		$this->assertArrayHasKey( 'message', $validation_result );
		$this->assertSame( $value, $validation_result['message'] );
	}

	/**
	 * Return true if a script/style is enqueued
	 *
	 * @param string $script_id id of the enqueued script/style.
	 * @param string $script_url url of the enqueued script/style.
	 * @param string $script_type it can be 'script' or 'style'.
	 * @return boolean
	 */
	public function script_is_enqueued( $script_id, $script_url, $script_type = 'script' ) {
		$script_array = [];
		global $wp_styles, $wp_scripts;
		$script_object;
		switch ( $script_type ) {
			case 'script':
				$script_object = $wp_scripts;
				break;
			case 'style':
				$script_object = $wp_styles;
				break;
		}

		foreach ( $script_object->queue as $script ) {
			if ( $script_object->registered[ $script ]->src === $script_url
				&& $script_id === $script ) {
				return true;
			}
		}

		return false;
	}

}
