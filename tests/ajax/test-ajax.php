<?php
/**
 * Class TestAJAX
 *
 * @package CEF
 * @group ajax
 * @runTestsInSeparateProcesses
 */
class TestAJAX extends WP_Ajax_UnitTestCase {

	/**
	 * Remove the filter for temporary tables
	 *
	 * @return void
	 */
	public function setup() {
		parent::setup();
		remove_filter( 'query', array( $this, '_create_temporary_tables' ) );
		remove_filter( 'query', array( $this, '_drop_temporary_tables' ) );
		wp_set_current_user( 1 );
	}

	/**
	 * Store dummy data
	 *
	 * @return array dummy data
	 */
	public function dummy_data() {
		global $wpdb;

		CEF()->activate();

		$data = array(
			array(
				'first_name' => 'Carlos',
				'last_name'  => 'Alvarez',
				'email'      => 'carlosenam@hotmail.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Dennet',
				'last_name'  => 'Bort',
				'email'      => 'dennet@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Fabiola',
				'last_name'  => 'Cats',
				'email'      => 'fabiola@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Enrique',
				'last_name'  => 'Dart',
				'email'      => 'enrique@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Gary',
				'last_name'  => 'Emac',
				'email'      => 'gary@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Harold',
				'last_name'  => 'Ferret',
				'email'      => 'harold@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Irene',
				'last_name'  => 'Garot',
				'email'      => 'Irene@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Jake',
				'last_name'  => 'Hermes',
				'email'      => 'jake@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Katerine',
				'last_name'  => 'Ipsos',
				'email'      => 'katerine@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Larry',
				'last_name'  => 'Jefrey',
				'email'      => 'larry@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Mery',
				'last_name'  => 'Lako',
				'email'      => 'mery@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
			array(
				'first_name' => 'Nancy',
				'last_name'  => 'Mongomery',
				'email'      => 'nancy@correo.com',
				'subject'    => 'Testing',
				'message'    => 'This is testing',
			),
		);

		$ed = new CEF_Enquiry_Data();

		foreach ( $data as $ed_data ) {
			$ed->set_data( $ed_data );
			$ed->save();
		}

		return $data;
	}

	/**
	 * Test process_form_data when all fields are valid
	 *
	 * @return void
	 */
	public function test_cef_form_all_fiels_are_valid() {
		$_POST['cef_first_name'] = 'Carlos';
		$_POST['cef_last_name']  = 'Alvarez';
		$_POST['cef_email']      = 'carlosenam@hotmail.com';
		$_POST['cef_subject']    = 'Testing';
		$_POST['cef_message']    = 'This is a test';
		$_POST['nonce']          = wp_create_nonce( 'cef_process_form_data' );

		CEF()->activate();
		try {
			wp_set_current_user( 1 );
			$this->_handleAjax( 'cef_process_form_data' );
		} catch ( WPAjaxDieContinueException $e ) {}
		$this->assertTrue( isset( $this->_last_response ) );
		$response = json_decode( $this->_last_response );
		$this->assertTrue( isset( $response->result ) );
		$this->assertSame( 'ok', $response->result );
	}

	/**
	 * Test process_form_data when some fields are invalid
	 *
	 * @return void
	 */
	public function test_cef_form_some_fiels_are_invalid() {
		$_POST['cef_first_name'] = 'Carlos';
		$_POST['cef_email']      = '@hotmail.com';
		$_POST['cef_subject']    = 'Testing';
		$_POST['cef_message']    = 'This is a test';
		$_POST['nonce']          = wp_create_nonce( 'cef_process_form_data' );

		CEF()->activate();
		try {
			wp_set_current_user( 1 );
			$this->_handleAjax( 'cef_process_form_data' );
		} catch ( WPAjaxDieContinueException $e ) {}
		$this->assertTrue( isset( $this->_last_response ) );
		$response = json_decode( $this->_last_response );
		$this->assertTrue( isset( $response->result ) );
		$this->assertSame( 'error', $response->result );
		$this->assertTrue( isset( $response->fields ) );
		$this->assertTrue( isset( $response->fields->cef_last_name ) );
		$this->assertSame( 'This field is required', $response->fields->cef_last_name );
		$this->assertTrue( isset( $response->fields->cef_email ) );
		$this->assertSame( 'Invalid Email', $response->fields->cef_email );
	}

	/**
	 * Test process_form_data when some fields are too long
	 *
	 * @return void
	 */
	public function test_cef_form_some_fiels_are_too_long() {
		$_POST['cef_first_name'] = 'asdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasd';
		$_POST['cef_email']      = 'asdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasd';
		$_POST['cef_subject']    = 'asdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasd';
		$_POST['cef_message']    = 'asdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasdasdsadsadasdsadsadasdasd';
		$_POST['nonce']          = wp_create_nonce( 'cef_process_form_data' );

		CEF()->activate();
		try {
			wp_set_current_user( 1 );
			$this->_handleAjax( 'cef_process_form_data' );
		} catch ( WPAjaxDieContinueException $e ) {}
		$this->assertTrue( isset( $this->_last_response ) );
		$response = json_decode( $this->_last_response );
		$this->assertTrue( isset( $response->result ) );
		$this->assertSame( 'error', $response->result );
		$this->assertTrue( isset( $response->fields ) );
		$this->assertTrue( isset( $response->fields->cef_first_name ) );
		$this->assertSame( 'Too long (100 chars max)', $response->fields->cef_first_name );
		$this->assertTrue( isset( $response->fields->cef_last_name ) );
		$this->assertSame( 'This field is required', $response->fields->cef_last_name );
		$this->assertTrue( isset( $response->fields->cef_email ) );
		$this->assertSame( 'Invalid Email', $response->fields->cef_email );
		$this->assertTrue( isset( $response->fields->cef_message ) );
		$this->assertSame( 'Too long (500 chars max)', $response->fields->cef_message );
	}

	/**
	 * Test get_enquiry_data when only page is defined
	 *
	 * @return void
	 */
	public function test_get_enquiry_data_only_page() {
		$this->dummy_data();

		$_POST['page']  = '1';
		$_POST['nonce'] = wp_create_nonce( 'cef-get-enquiry-data' );

		try {
			wp_set_current_user( 1 );
			$this->_handleAjax( 'cef_get_enquiry_data' );
		} catch ( WPAjaxDieContinueException $e ) {}
		$this->assertTrue( isset( $this->_last_response ) );
		$response = json_decode( $this->_last_response );
		$this->assertTrue( isset( $response ) );
		$this->assertSame( 2, $response->pages );
		$this->assertCount( 10, $response->entries );
	}

	/**
	 * Test get
	 *
	 * @return void
	 */
	public function test_get() {
		global $wpdb;

		CEF()->activate();

		$data = array(
			'first_name' => 'Carlos',
			'last_name'  => 'Alvarez',
			'email'      => 'carlosenam@hotmail.com',
			'subject'    => 'Testing',
			'message'    => 'This is testing',
		);

		$ed = new CEF_Enquiry_Data();
		$ed->set_data( $data );
		$new_entry_id = $ed->save();

		$_POST['id']    = $new_entry_id;
		$_POST['nonce'] = wp_create_nonce( 'cef-get-entry' );

		try {
			$this->_handleAjax( 'cef_get_enquiry_data_entry' );
		} catch ( WPAjaxDieContinueException $e ) {}
		$this->assertTrue( isset( $this->_last_response ) );
		$response = json_decode( $this->_last_response );
		$this->assertTrue( isset( $response ) );
		$this->assertSame( $new_entry_id, intval( $response->id ) );
		$this->assertSame( $data['first_name'], $response->first_name );
		$this->assertSame( $data['last_name'], $response->last_name );
		$this->assertSame( $data['email'], $response->email );
		$this->assertSame( $data['subject'], $response->subject );
		$this->assertSame( $data['message'], $response->message );
	}


	/**
	 * Test get when id is not defined
	 *
	 * @return void
	 */
	public function test_get_when_id_is_not_defined() {
		global $wpdb;

		CEF()->activate();

		$data = array(
			'first_name' => 'Carlos',
			'last_name'  => 'Alvarez',
			'email'      => 'carlosenam@hotmail.com',
			'subject'    => 'Testing',
			'message'    => 'This is testing',
		);

		$ed = new CEF_Enquiry_Data();
		$ed->set_data( $data );
		$new_entry_id = $ed->save();

		$_POST['nonce'] = wp_create_nonce( 'cef-get-entry' );

		try {
			$this->_handleAjax( 'cef_get_enquiry_data_entry' );
		} catch ( WPAjaxDieContinueException $e ) {}
		$this->assertTrue( isset( $this->_last_response ) );
		$response = json_decode( $this->_last_response );
		$this->assertTrue( isset( $response ) );
		$this->assertTrue( isset( $response->data ) );
		$this->assertSame( 'This param is not valid: id', $response->data );
	}

	/**
	 * Test get when id is not valid
	 *
	 * @return void
	 */
	public function test_get_when_id_is_not_valid() {
		global $wpdb;

		CEF()->activate();

		$data = array(
			'first_name' => 'Carlos',
			'last_name'  => 'Alvarez',
			'email'      => 'carlosenam@hotmail.com',
			'subject'    => 'Testing',
			'message'    => 'This is testing',
		);

		$ed = new CEF_Enquiry_Data();
		$ed->set_data( $data );
		$new_entry_id = $ed->save();

		$_POST['id']    = 'HOLA<script>alert("hello");</script>';
		$_POST['nonce'] = wp_create_nonce( 'cef-get-entry' );

		try {
			$this->_handleAjax( 'cef_get_enquiry_data_entry' );
		} catch ( WPAjaxDieContinueException $e ) {}
		$this->assertTrue( isset( $this->_last_response ) );
		$response = json_decode( $this->_last_response );
		$this->assertTrue( isset( $response ) );
		$this->assertTrue( isset( $response->data ) );
		$this->assertSame( 'This param is not valid: id', $response->data );
	}
}
