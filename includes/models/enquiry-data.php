<?php
/**
 * Class CEF_Enquiry_Data
 *
 * @package CEF
 */

// Prevents direct access to this file.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * Represents a row in the 'cef_form_data' database table
 */
class CEF_Enquiry_Data {

	/**
	 * Enquiry data
	 *
	 * @var array
	 */
	private $data = array(
		'first_name' => '',
		'last_name'  => '',
		'email'      => '',
		'subject'    => '',
		'message'    => '',
	);

	/**
	 * Set the initial enquiry data
	 *
	 * @param array $data Array containing the enquiry data.
	 * @return void
	 */
	public function __construct( $data = array() ) {
		if ( ! empty( $data ) ) {
			$this->set_data( $data );
		}
	}

	/**
	 * Set the enquiry data
	 *
	 * @param array $data Array containing the enquiry data.
	 * @return void
	 * @throws Exception When $data is not an array.
	 */
	public function set_data( $data ) {
		if ( gettype( $data ) !== 'array' ) {
			throw new Exception( '$data is not an array' );
		}

		foreach ( $this->data as $key => $value ) {
			if ( isset( $data[ $key ] ) ) {
				$this->data[ $key ] = $data[ $key ];
			}
		}
	}

	/**
	 * Saves a enquiry data entry in the database
	 *
	 * @return int ID of the inserted row
	 * @throws Exception When the enquiry data can be saved in the database.
	 */
	public function save() {
		global $wpdb;

		CEF()->activate();

		$data_to_save                = $this->data;
		$data_to_save['date'] = current_time( 'Y-m-d H:i:s' );

		$enquiry_data_id = $wpdb->insert( CEF()->get_table_name(), $data_to_save );
		if ( ! $enquiry_data_id ) {
			throw new Exception( 'Database error: Enquiry data could not be saved' );
		}

		return $wpdb->insert_id;
	}

	/**
	 * Get the enquiry data entries from the database.
	 *
	 * @param integer $page Number of the page.
	 * @param string  $orderby Name of the field to be used to order the entries.
	 * @param string  $order It could be 'ASC' or 'DESC'.
	 * @return array  The entries in the given page.
	 */
	public static function get_enquiry_data( $page = 1, $orderby = 'date', $order = 'DESC' ) {
		global $wpdb;

		$entries_per_page = 10;

		$offset = ( $page - 1 ) * $entries_per_page;

		$sql = "SELECT id, email, subject, message FROM " . CEF()->get_table_name() . " ORDER BY $orderby $order LIMIT $offset,$entries_per_page";

		$enquiry_data_entries = $wpdb->get_results( $sql, ARRAY_A );

		if ( ! $enquiry_data_entries ) {
			return array();
		}

		$enquiry_data_entries = array_map(
			function( $enquiry_data_entry ) {
				$enquiry_data_entry['message'] = wp_trim_words( $enquiry_data_entry['message'], 10, '...' );
				return $enquiry_data_entry;
			},
			$enquiry_data_entries
		);

		return $enquiry_data_entries;
	}

	/**
	 * Get the number of pages in the database.
	 * (every page have 10 entries)
	 *
	 * @return integer Number of pages.
	 */
	public static function get_number_of_pages() {
		global $wpdb;
		$enquiry_data_count = $wpdb->get_var( sprintf( 'SELECT COUNT(id) FROM %s', CEF()->get_table_name() ) );

		return intval( ceil( $enquiry_data_count / 10 ) );
	}

	/**
	 * Gets the enquiry data entry given its ID
	 *
	 * @param integer $row_id Row Id.
	 * @return array Array containing the enquiry data
	 */
	public static function get( $row_id ) {
		global $wpdb;
		$sql      = sprintf( 'SELECT * FROM %s WHERE id = %d', CEF()->get_table_name(), $row_id );
		$row_data = $wpdb->get_row( $sql, ARRAY_A );
		if ( ! $row_data ) {
			return array();
		}

		return $row_data;
	}
}
