<?php
/**
 * Class CEF_Form_Shortcode
 *
 * @package CEF
 */

// Prevents direct access to this file.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * Handles the [cef_form] shortcode
 */
class CEF_Form_Shortcode {

	/**
	 * Add the shortcode and register the ajax callback
	 *
	 * @return void
	 */
	public function __construct() {
		add_shortcode( 'cef_form', array( $this, 'html_output' ) );
		add_action( 'wp_ajax_cef_process_form_data', array( $this, 'process_form_data' ) );
		add_action( 'wp_ajax_nopriv_cef_process_form_data', array( $this, 'process_form_data' ) );
	}

	/**
	 * Shortcode HTML output
	 *
	 * @return string HTML output
	 */
	public function html_output() {
		// JS and CSS files are enqueued only when the shortcode is being used.
		$this->enqueue_scripts_and_styles();

		$current_user_first_name = '';
		$current_user_last_name  = '';
		$current_user_email      = '';

		$logged_in_user = wp_get_current_user();

		if ( $logged_in_user->ID ) {
			$current_user_first_name = $logged_in_user->first_name;
			$current_user_last_name  = $logged_in_user->last_name;
			$current_user_email      = $logged_in_user->user_email;
		}

		$enquiry_form_id = CEF()->generate_unique_id();

		ob_start();
		?>
		<div class="cef_enquiry_form" id="<?php echo esc_attr( $enquiry_form_id ); ?>">
			<div class="cef_loader hidden">
				<div class="cef_spinner"></div>
			</div>
			<div class="cef_result hidden">
				<div class="cef_result_msg">
					<p></p>
					<span class="cef_back">
						<?php echo esc_html__( 'Back', 'cef' ); ?>
					</span>
				</div>
			</div>
			<div class="cef_form_wrapper">
				<form class="cef_form">
					<div class="cef_form_title">
						<?php echo esc_html__( 'Submit your feedback', 'cef' ); ?>
					</div>
					<div class="cef_fields">
						<div class="cef_field">
							<label for="cef_first_name"><?php echo esc_html__( 'First Name', 'cef' ); ?></label>
							<input type="text" id="cef_first_name" value="<?php echo esc_attr( $current_user_first_name ); ?>" />
						</div>
						<div class="cef_field">
							<label for="cef_last_name"><?php echo esc_html__( 'Last Name', 'cef' ); ?></label>
							<input type="text" id="cef_last_name" value="<?php echo esc_attr( $current_user_last_name ); ?>" />
						</div>
						<div class="cef_field">
							<label for="cef_email"><?php echo esc_html__( 'Email', 'cef' ); ?></label>
							<input type="email" id="cef_email" value="<?php echo esc_attr( $current_user_email ); ?>" />
						</div>
						<div class="cef_field">
							<label for="cef_subject"><?php echo esc_html__( 'Subject', 'cef' ); ?></label>
							<input type="text" id="cef_subject" />
						</div>
						<div class="cef_field">
							<label for="cef_message"><?php echo esc_html__( 'Message', 'cef' ); ?></label>
							<textarea id="cef_message"></textarea>
						</div>
						<div class="cef_submit_wrapper">
							<button class="cef_submit_button"><?php echo esc_html__( 'Submit', 'cef' ); ?></button>
						</div>
				</form>
				</div>
			</div>
		</div>
		<?php
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}

	/**
	 * Enqueues the scripts and styles.
	 *
	 * @return void
	 */
	public function enqueue_scripts_and_styles() {
		wp_enqueue_script( 'cef-form', CEF_ASSETS_URL . 'js/cef-form.js', array( 'jquery' ), CEF_VERSION, true );
		wp_enqueue_style( 'cef-form-styles', CEF_ASSETS_URL . 'css/cef-form.css', null, CEF_VERSION );

		wp_localize_script(
			'cef-form',
			'CEFFormVars',
			array(
				'ajaxUrl'           => admin_url( 'admin-ajax.php' ),
				'nonce'             => wp_create_nonce( 'cef-process-form-data' ),
				'successText'       => esc_html__( 'Thank you for sending us your feedback', 'cef' ),
				'systemErrorText'   => esc_html__( 'System error: Please try again later', 'cef' ),
				'requiredFieldText' => esc_html__( 'This field is required', 'cef' ),
				'tooLong500Text'    => esc_html__( 'Too long (500 chars max)', 'cef' ),
				'tooLong100Text'    => esc_html__( 'Too long (100 chars max)', 'cef' ),
				'invalidEmailText'  => esc_html__( 'Invalid Email', 'cef' ),
			)
		);
	}

	/**
	 * Validates a single form field
	 *
	 * @param string $name Id of the form field.
	 * @param string $value Value of the field.
	 * @return array If the value of the field is valid then it returns an array
	 * with the validation result and the value itself, otherwise it return
	 * the validation message.
	 *
	 * Example:
	 * Invalid field => array( 'result' => 'error', 'message' => 'This field is required' )
	 * Valid field => array( 'result' => 'ok', 'message' => 'Alvarez' )
	 */
	public function validate_single_form_field( $name, $value ) {

		$result = array(
			'result'  => 'ok',
			'message' => $value,
		);

		switch ( $name ) {
			case 'first_name':
			case 'last_name':
			case 'subject':
			case 'message':
				if ( empty( $value ) ) {
					$result['result']  = 'error';
					$result['message'] = esc_html__( 'This field is required', 'cef' );
				} elseif ( 'message' === $name ) {
					if ( strlen( $value ) > 500 ) {
						$result['result']  = 'error';
						$result['message'] = esc_html__( 'Too long (500 chars max)', 'cef' );
					}
				} elseif ( strlen( $value ) > 100 ) {
					$result['result']  = 'error';
					$result['message'] = esc_html__( 'Too long (100 chars max)', 'cef' );
				}
				break;
			case 'email':
				if ( empty( $value ) ) {
					$result['result']  = 'error';
					$result['message'] = esc_html__( 'This field is required', 'cef' );
				} elseif ( ! is_email( $value ) ) {
					$result['result']  = 'error';
					$result['message'] = esc_html__( 'Invalid Email', 'cef' );
				} elseif ( strlen( $value ) > 100 ) {
					$result['result']  = 'error';
					$result['message'] = esc_html__( 'Too long (100 chars max)', 'cef' );
				}
				break;
		}

		return $result;
	}

	/**
	 * Validates and sanitizes the data from the [cef_form] form.
	 *
	 * @param array $post_data This array corresponds to $_POST.
	 * @return array If there are invalid field values then it returns
	 * an array with validation result and the validation
	 * messages, otherwise it returns the field values (with no 'cef_' sufix)
	 *
	 * Example:
	 * Invalid Fields => array( 'result' => 'error', 'fields' => array( 'cef_first_name' => 'This field is required', ... ) )
	 * Valid field => array( 'result' => 'ok', 'fields' => array( 'first_name' => 'Carlos', ... ) )
	 */
	public function validate_form_data( $post_data ) {
		$form_fields = array(
			'result' => 'ok',
			'fields' => array(
				'first_name' => isset( $post_data['cef_first_name'] ) ? sanitize_text_field( $post_data['cef_first_name'] ) : '',
				'last_name'  => isset( $post_data['cef_last_name'] ) ? sanitize_text_field( $post_data['cef_last_name'] ) : '',
				'email'      => isset( $post_data['cef_email'] ) ? $post_data['cef_email'] : '',
				'subject'    => isset( $post_data['cef_subject'] ) ? sanitize_text_field( $post_data['cef_subject'] ) : '',
				'message'    => isset( $post_data['cef_message'] ) ? sanitize_textarea_field( $post_data['cef_message'] ) : '',
			),
		);

		$validation_messages = array(
			'result' => 'error',
			'fields' => array(),
		);

		$fields = $form_fields['fields'];

		foreach ( $fields as $field_key => $field_value ) {
			$field_validation_result = $this->validate_single_form_field( $field_key, $field_value );

			if ( 'error' === $field_validation_result['result'] ) {
				$validation_messages['fields'][ 'cef_' . $field_key ] = $field_validation_result['message'];
			}
		}

		if ( ! empty( $validation_messages['fields'] ) ) {
			return $validation_messages;
		}

		return $form_fields;
	}

	/**
	 * Proccesses the data sent from the form
	 *
	 * @return void
	 */
	public function process_form_data() {
		check_ajax_referer( 'cef-process-form-data', 'nonce', false );

		$validation_result = $this->validate_form_data( $_POST );

		if ( 'error' === $validation_result['result'] ) {
			wp_send_json( $validation_result );
		}

		$enquiry_data = new CEF_Enquiry_Data( $validation_result['fields'] );

		$enquiry_data->save();

		wp_send_json(
			array(
				'result' => 'ok',
			)
		);
	}

}
