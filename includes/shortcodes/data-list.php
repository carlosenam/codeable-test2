<?php
/**
 * Class CEF_DataList_Shortcode
 *
 * @package CEF
 */

// Prevents direct access to this file.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * Handles the [cef_data_list] shortcode
 */
class CEF_DataList_Shortcode {

	/**
	 * Add the shortcode and register the ajax callback
	 */
	public function __construct() {
		add_shortcode( 'cef_data_list', array( $this, 'html_output' ) );
		add_action( 'wp_ajax_cef_get_enquiry_data', array( $this, 'get_enquiry_data' ) );
		add_action( 'wp_ajax_cef_get_enquiry_data_entry', array( $this, 'get_enquiry_data_entry' ) );
	}

	/**
	 * Shortcode HTML output
	 *
	 * @return string HTML output
	 */
	public function html_output() {
		// JS and CSS files are enqueued only when the shortcode is being used.
		$this->enqueue_scripts_and_styles();

		$data_list_id = CEF()->generate_unique_id();

		ob_start();
		?>
		<div class="cef_data_list" id="<?php echo esc_attr( $data_list_id ); ?>">
				<div class="cef_loader hidden">
					<div class="cef_spinner"></div>
				</div>
			<div class="cef_list_wrapper">
				<table class="cef_data_table">
					<thead>
						<tr>
							<th>
								#
							</th>
							<th class="cef_sort" orderby="email">
								<?php echo esc_html__( 'Email', 'cef' ); ?>
							</th>
							<th>
								<?php echo esc_html__( 'Subject', 'cef' ); ?>
							</th>
							<th>
								<?php echo esc_html__( 'Message', 'cef' ); ?>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="4" class="cef_empty_list">
								<?php echo esc_html__( 'NO ENTRIES', 'cef' ); ?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="cef_pages">
				<span><?php echo esc_html__( 'Page', 'cef' ); ?>:</span>
				<select class="cef_pages_selector" disabled></select>
			</div>
			<div class="cef_single_entry_wrapper">
				<div class="cef_single_title">
					<?php echo esc_html__( 'Detailed Info', 'cef' ); ?>
				</div>
				<div class="cef_single_field">
					<p class="cef_field_label"><?php echo esc_html__( 'Date', 'cef' ); ?></p>
					<p class="cef_date"></p>
				</div>
				<div class="cef_single_field">
					<p class="cef_field_label"><?php echo esc_html__( 'First Name', 'cef' ); ?></p>
					<p class="cef_first_name"></p>
				</div>
				<div class="cef_single_field">
					<p class="cef_field_label"><?php echo esc_html__( 'Last Name', 'cef' ); ?></p>
					<p class="cef_last_name"></p>
				</div>
				<div class="cef_single_field">
					<p class="cef_field_label"><?php echo esc_html__( 'Email', 'cef' ); ?></p>
					<p class="cef_email"></p>
				</div>
				<div class="cef_single_field">
					<p class="cef_field_label"><?php echo esc_html__( 'Subject', 'cef' ); ?></p>
					<p class="cef_subject"></p>
				</div>
				<div class="cef_single_field">
					<p class="cef_field_label"><?php echo esc_html__( 'Message', 'cef' ); ?></p>
					<p class="cef_message"></p>
				</div>
			</div>
		</div>
		<?php
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}

	/**
	 * Enqueues the scripts and styles.
	 *
	 * @return void
	 */
	public function enqueue_scripts_and_styles() {
		wp_enqueue_script( 'cef-data-list', CEF_ASSETS_URL . 'js/cef-data-list.js', array( 'jquery' ), CEF_VERSION, true );
		wp_enqueue_style( 'cef-data-list-styles', CEF_ASSETS_URL . 'css/cef-data-list.css', null, CEF_VERSION );

		wp_localize_script(
			'cef-data-list',
			'CEFDataListVars',
			array(
				'ajaxUrl'       => admin_url( 'admin-ajax.php' ),
				'nonceFilter'   => wp_create_nonce( 'cef-get-enquiry-data' ),
				'nonceGetEntry' => wp_create_nonce( 'cef-get-entry' ),
				'systemErrorText' => esc_html__( 'System Error', 'cef' ),
			)
		);
	}

	/**
	 * Validate the params in the $_POST array
	 *
	 * @param array  $post_data This array corresponds to $_POST.
	 * @param string $action It can be 'cef-filter-data' or 'cef-get-entry'.
	 * @return array|string If any of the post params are incorrect or undefined
	 * then an error message is returned otherwise an array with the correct params
	 * is returned.
	 */
	public function validate_params( $post_data, $action = 'cef-filter-data' ) {

		if ( 'cef-get-entry' === $action ) {
			$entry_id = isset( $post_data['id'] ) ? $post_data['id'] : '';
			if ( ! is_numeric( $entry_id ) ) {
				return 'This param is not valid: id';
			}

			return array(
				'id' => intval( $entry_id ),
			);
		}

		$valid_params = array(
			'page'    => isset( $post_data['page'] ) ? $post_data['page'] : '1',
			'orderby' => isset( $post_data['orderby'] ) ? $post_data['orderby'] : 'date',
			'order'   => isset( $post_data['order'] ) ? $post_data['order'] : 'DESC',
		);

		$orderby_values = array(
			'email',
			'date',
		);

		$error_message = 'This param is not valid: ';

		foreach ( $valid_params as $key => $value ) {
			switch ( $key ) {
				case 'page':
					if ( ! is_numeric( $value ) ) {
						return $error_message . $key;
					}
					break;

				case 'orderby':
					if ( ! in_array( $value, $orderby_values, true ) ) {
						return $error_message . $key;
					}
					break;

				case 'order':
					if ( 'ASC' !== $value && 'DESC' !== $value ) {
						return $error_message . $key;
					}
					break;
			}
		}

		return $valid_params;
	}

	/**
	 * AJAX callback for the 'cef_get_enquiry_data' action. Validates the params
	 * in $_POST, in the params are incorrect it throws an error with code 500.
	 * If the params are correct then it sends the entries alojg with number of pages.
	 *
	 * @return void
	 */
	public function get_enquiry_data() {
		check_ajax_referer( 'cef-get-enquiry-data', 'nonce', false );

		$validation_result = $this->validate_params( $_POST );

		if ( 'string' === gettype( $validation_result ) ) {
			wp_send_json_error( $validation_result, 500 );
		}

		$enquiry_data_entries = CEF_Enquiry_Data::get_enquiry_data(
			$validation_result['page'],
			$validation_result['orderby'],
			$validation_result['order']
		);

		$number_of_pages = CEF_Enquiry_Data::get_number_of_pages();

		wp_send_json(
			array(
				'pages'   => $number_of_pages,
				'entries' => $enquiry_data_entries,
			)
		);
	}

	/**
	 * AJAX callback for the 'cef_get_enquiry_data_entry' action.
	 * Validates that $_POST contains the 'id' param. If the
	 * 'id' param is invalid or undefined the it throws a error
	 * with code 500. If the 'id' param is valid, then it sends
	 * the data of the entry with that id.
	 *
	 * @return void
	 */
	public function get_enquiry_data_entry() {
		check_ajax_referer( 'cef-get-entry', 'nonce', false );

		$validation_result = $this->validate_params( $_POST, 'cef-get-entry' );
		if ( 'string' === gettype( $validation_result ) ) {
			wp_send_json_error( $validation_result, 500 );
		}

		$enquiry_data_entry = CEF_Enquiry_Data::get( $validation_result['id'] );
		wp_send_json( $enquiry_data_entry );
	}
}
