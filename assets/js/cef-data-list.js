var $ = jQuery;
var CEFFiltersValues = {}; // This object contains the filter values of every data list.

/**
 * Returns a selector string for a given data list.
 *
 * @param {string} dataListId ID of a data list
 * @returns string
 */
function CEFGetDataListSelector(dataListId) {
  return '.cef_data_list#' + dataListId + ' ';
}

/**
 * Shows the loader for a data list
 *
 * @param {string} dataListId ID of an data list.
 */
function CEFShowLoaderDataList(dataListId) {
  $(CEFGetDataListSelector(dataListId) + '.cef_loader').removeClass('hidden');
}

/**
 * Hides the loader for a data list
 *
 * @param {string} dataListId ID of an data list.
 */
function CEFHideLoaderDataList(dataListId) {
  $(CEFGetDataListSelector(dataListId) + '.cef_loader').addClass('hidden');
}

/**
 * Make the AJAX Request
 *
 * @param {string} dataListId ID of the data list filled in the success callback
 * @param {string} action it can be 'get_data' or 'get_entry'
 * @param {function} onSuccess Called when the ajax request is successful
 * @returns void
 */
function CEFAjaxRequest(dataListId, action, onSuccess) {
  var filters = CEFFiltersValues[dataListId];
  var AJAXData = {};
  if (action === 'get_data') {
    AJAXData = $.extend(true, {}, filters);
    AJAXData.action = 'cef_get_enquiry_data';
    AJAXData.nonce = CEFDataListVars.nonceFilter;
  } else if (action === 'get_entry') {
    AJAXData.id = filters.id; // ID of the enquiry data entry in the database.
    AJAXData.action = 'cef_get_enquiry_data_entry';
    AJAXData.nonce = CEFDataListVars.nonceGetEntry;
  }

  CEFShowLoaderDataList(dataListId);

  $.ajax({
    url: CEFDataListVars.ajaxUrl,
    type: 'post',
    data: AJAXData,
    dateType: 'json',
    timeout: 30000,
    success: function (response) {
      if(action === 'get_data') {
        onSuccess(response.entries, parseInt(response.pages), filters.page, dataListId);
      } else if (action === 'get_entry'){
        onSuccess(response, dataListId );
      }
      CEFHideLoaderDataList(dataListId);
    },
    error: function (error, status, errorMessage) {
      console.log(errorMessage);
      CEFOnErrorDataList();
    },
  });
}

/**
 * Fills a data list and its pages selector.
 * Also attaches click event handlers for every entry in the list.
 *
 * @param {array} entries Array containing the entries
 * @param {integer} pages Number of pages.
 * @param {integer} page Number of the current page
 * @param {string} dataListId ID of the data list which will be filled
 * @returns void
 */
function CEFFillList(entries, pages, page, dataListId) {
  // Filling the pages selector
  if (pages) {
    var pagesSelector = $(CEFGetDataListSelector(dataListId) + '.cef_pages_selector');
    pagesSelector.find('option').remove();
    for (var j = 1; j <= pages; j += 1) {
      var option = $('<option value="' + j + '" >' + j + '</option>');
      pagesSelector.append(option);
    }
    pagesSelector.removeAttr('disabled').val(page);
  }

  var tableBody = $(CEFGetDataListSelector(dataListId) + '.cef_data_table tbody');
  var numberOfEntries = entries.length;

  if (!numberOfEntries) {
    return;
  }

  tableBody.find('tr').remove();

  var offset = (page - 1) * 10;
  for (var i = 0; i < numberOfEntries; i += 1) {
    var tableRow = $('<tr></tr>');
    tableRow.append('<td>' + ( i + 1 + offset ) + '</td>'); // '#' column
    tableRow.append('<td>' + entries[i].email + '</td>'); // 'Email' column
    tableRow.append('<td>' + entries[i].subject + '</td>'); // 'Subject' column
    tableRow.append('<td>' + entries[i].message + '</td>'); // 'Message' column

    // Attaching click event handlers to this entry

    tableRow.click((function (entryId, tableRow) {
      return function() {
        tableBody.find('tr').removeClass('active');
        tableRow.addClass('active');
        CEFFiltersValues[dataListId].id = entryId;
        CEFAjaxRequest(dataListId, 'get_entry', CEFFillDetailedInfo);
      };
    })(entries[i].id, tableRow));

    tableBody.append(tableRow);
  }
}

/**
 * Fill the 'Detailed Info' section
 * with an entry
 *
 * @param {object} entryData Object with entry data
 * @param {string} dataListId Id of a data list which 'Detailed Info' section will be filled
 * @returns void
 */
function CEFFillDetailedInfo(entryData, dataListId) {
    $(CEFGetDataListSelector(dataListId) + '.cef_date').html(entryData.date);
    $(CEFGetDataListSelector(dataListId) + '.cef_first_name').html(entryData.first_name);
    $(CEFGetDataListSelector(dataListId) + '.cef_last_name').html(entryData.last_name);
    $(CEFGetDataListSelector(dataListId) + '.cef_email').html(entryData.email);
    $(CEFGetDataListSelector(dataListId) + '.cef_subject').html(entryData.subject);
    $(CEFGetDataListSelector(dataListId) + '.cef_message').html(entryData.message);
}

/**
 * Callback for failed AJAX request
 *
 * @param {object} respone AJAX response.
 * @returns void
 */
function CEFOnErrorDataList() {
  alert(CEFDataListVars.systemErrorText);
}

/**
 * Set the filters values in the global scope
 * for a specified data list
 *
 * @param {string} dataListId ID of the data list.
 * @returns void
 */
function CEFSetFiltersValues(dataListId) {
  CEFFiltersValues[dataListId] = {
    page: 1,
    orderby: 'date',
    order: 'DESC',
  };
}

/**
 * Toggles the order attribute of a sort button element.
 * The new order and the orderby attribute are updated in CEFFiltersValues.
 * The order attribute only can have three values: 'ASC', 'DESC' and ''
 *
 * When the order value is '', then the filter parameters change to default.
 *
 * @param {HTML Element} sortEl Sort Button
 * @param {string} dataListId ID of the currently active data list
 * @return void
 */
function CEFToggleOrder(sortEl, dataListId) {
  var orderby = $(sortEl).attr('orderby');
  var order = $(sortEl).attr('order');
  var newOrder = '';

  switch (order) {
    case '':
    case undefined:
      newOrder = 'ASC';
      break;

    case 'ASC':
      newOrder = 'DESC';
      break;

    case 'DESC':
      newOrder = '';
      break;

    default:
      newOrder = '';
      break;
  }

  $(sortEl).attr('order', newOrder);

  if (!newOrder) {
    CEFFiltersValues[dataListId].order = 'DESC';
    CEFFiltersValues[dataListId].orderby = 'date';
  } else {
    CEFFiltersValues[dataListId].order = newOrder;
    CEFFiltersValues[dataListId].orderby = orderby;
  }
}

/**
 * Attach the event handlers and get
 * the first page of the enquiry data
 *
 * @return void
 */
function CEFinitializeDataList() {
  $('.cef_data_list').each(function () {
    var dataListId = this.id;
    CEFSetFiltersValues(dataListId);
    $('.cef_data_list#' + dataListId + ' .cef_sort').click(function () {
      CEFToggleOrder(this, dataListId);
      CEFAjaxRequest(dataListId, 'get_data', CEFFillList);
    });

    $('.cef_data_list#' + dataListId + ' .cef_pages_selector').change(function () {
      CEFFiltersValues[dataListId].page = $(this).val();
      CEFAjaxRequest(dataListId, 'get_data', CEFFillList);
    });

    CEFAjaxRequest(dataListId, 'get_data', CEFFillList);
  });
}

$(document).ready(CEFinitializeDataList);
