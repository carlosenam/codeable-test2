var $ = jQuery;
var CEFActiveFormId = ''; // Id of the currently active enquiry form.
var CEFActiveFormSelector = ''; // Selector of the currently active form.

/**
 * Set the Id of the active form
 *
 * @param {string} cefFormId ID of the active form
 */
function setActiveFormId(cefFormId) {
  CEFActiveFormId = cefFormId;
  CEFActiveFormSelector = '.cef_enquiry_form#' + cefFormId + ' ';
}

/**
 * Returns true if email is a valid email string, false otherwise.
 *
 * @param {string} email email to be evaluated.
 * @returns boolean
 */
function CEFIsEmail(email) {
  var trimmedEmail = email.trim();
  if (trimmedEmail.match(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}/)) {
    return true;
  }
  return false;
}

/**
 * Validates a single form field
 *
 * @param {string} id Id of the field element
 * @param {string} value Value of the field element
 *
 * @return {object} if the field value is valid then it returns an object
 * with the field valud, otherwise it returns an object with the
 * validation message.
 *
 * Example:
 * Valid field => { result: 'ok', message: 'Carlos' }
 * Invalid field => { result: 'error', message: 'This field is required' }
 */
function CEFValidateField(id, value) {
  var result = {
    result: 'ok',
    message: value,
  };

  switch (id) {
    case 'cef_first_name':
    case 'cef_last_name':
    case 'cef_subject':
    case 'cef_message':
      if (!value) {
        result.result = 'error';
        result.message = CEFFormVars.requiredFieldText;
      } else if (id === 'cef_message') {
        if (value.length > 500) {
          result.result = 'error';
          result.message = CEFFormVars.tooLong500Text;
        }
      } else if (value.length > 100) {
        result.result = 'error';
        result.message = CEFFormVars.tooLong100Text;
      }
      break;

    case 'cef_email':
      if (!value) {
        result.result = 'error';
        result.message = CEFFormVars.requiredFieldText;
      } else if (!CEFIsEmail(value)) {
        result.result = 'error';
        result.message = CEFFormVars.invalidEmailText;
      } else if (value.length > 100) {
        result.result = 'error';
        result.message = CEFFormVars.tooLong100Text;
      }
      break;

    default:
      break;
  }

  return result;
}

/**
 * Validates the form fields
 *
 * @returns {object} if there are invalid field values
 * then it returns an object with the validation messages,
 * otherwise it returns an object with the field values.
 *
 * Example:
 * Valid fields => { result: 'ok', fields: { cef_first_name: 'Carlos'.... } }
 * Invalid fields => { result: 'error', fields: { cef_first_name: 'This field is required'... } }
 */
function CEFValidateFields() {
  var fieldValues = {
    result: 'ok',
    fields: {},
  };

  var errorMessages = {
    result: 'error',
    fields: {},
  };

  fieldValues.fields.cef_first_name = $(CEFActiveFormSelector + '#cef_first_name').val();
  fieldValues.fields.cef_last_name = $(CEFActiveFormSelector + '#cef_last_name').val();
  fieldValues.fields.cef_email = $(CEFActiveFormSelector + '#cef_email').val();
  fieldValues.fields.cef_subject = $(CEFActiveFormSelector + '#cef_subject').val();
  fieldValues.fields.cef_message = $(CEFActiveFormSelector + '#cef_message').val();

  var fieldEntries = Object.entries(fieldValues.fields);
  var validationResult = {};

  for (var i = 0; i < fieldEntries.length; i += 1) {
    var fieldId = fieldEntries[i][0];
    var fieldValue = fieldEntries[i][1];

    validationResult = CEFValidateField(fieldId, fieldValue);
    switch (validationResult.result) {
      case 'error':
        errorMessages.fields[fieldId] = validationResult.message;
        break;
      case 'ok':
      default:
        break;
    }
  }

  if (Object.keys(errorMessages.fields).length) {
    return errorMessages;
  }

  return fieldValues;
}

/**
 * Sends the form data to the server
 *
 * @param {string} ajaxUrl WP AJAX url
 * @param {object} formData Form data
 * @param {function} onSuccess Called when the ajax request is successful
 * @param {function} onError Called when the ajax request fails
 * @returns void
 */
function CEFSendData(ajaxUrl, formData, onSuccess, onError) {
  $.ajax({
    url: ajaxUrl,
    type: 'post',
    data: formData,
    dateType: 'json',
    timeout: 30000,
    success: onSuccess,
    error: function (error, status, errorMessage) {
      console.log(errorMessage);
      onError(CEFFormVars.systemErrorText, 'cef_system_error');
    },
  });
}

/**
 * Shows the loader
 *
 * @returns void
 */
function CEFShowLoader() {
  $(CEFActiveFormSelector + '.cef_loader').removeClass('hidden');
}

/**
 * Hides the loader
 *
 * @returns void
 */
function CEFHideLoader() {
  $(CEFActiveFormSelector + '.cef_loader').addClass('hidden');
}

/**
 * Shows the result message
 *
 * @param {string} message Message to be shown in the result.
 * @param {string} messageClass CSS class assigned to the container element of the message.
 * @returns void
 */
function CEFShowResult(message, messageClass) {
  $(CEFActiveFormSelector + '.cef_result').removeClass('hidden');
  $(CEFActiveFormSelector + '.cef_result p').html(message);
  $(CEFActiveFormSelector + '.cef_result p').attr('class', messageClass);
  $(CEFActiveFormSelector + '.cef_form_wrapper').addClass('hidden');
}

/**
 * Hides the result message
 *
 * @param {string} cefFormId
 * @returns void
 */
function CEFHideResult(cefFormId) {
  setActiveFormId(cefFormId);
  $(CEFActiveFormSelector + '.cef_result').addClass('hidden');
  $(CEFActiveFormSelector + '.cef_form_wrapper').removeClass('hidden');
}

/**
 * Resets the form
 *
 * @returns void
 */
function CEFResetForm() {
  $(CEFActiveFormSelector + '#cef_first_name').val('');
  $(CEFActiveFormSelector + '#cef_last_name').val('');
  $(CEFActiveFormSelector + '#cef_email').val('');
  $(CEFActiveFormSelector + '#cef_subject').val('');
  $(CEFActiveFormSelector + '#cef_message').val('');
  $(CEFActiveFormSelector + '.cef_error').remove();
}

/**
 * Shows the validation error messages below every invalid field
 *
 * @param {object} Object containing the error messages
 * @returns void
 */
function CEFShowErrors(errorMessages) {
  // Remove existing error messages
  $(CEFActiveFormSelector + '.cef_error').remove();

  const arrayErrorMessages = Object.entries(errorMessages);

  for (var i = 0; i < arrayErrorMessages.length; i += 1) {
    var fieldId = arrayErrorMessages[i][0];
    var errorMessage = arrayErrorMessages[i][1];

    $(CEFActiveFormSelector + '#' + fieldId).after('<p class="cef_error">' + errorMessage + '</p>');
  }
}

/**
 * AJAX success callback
 *
 * @param {object} response Object containing the response from the server.
 * @returns void
 */
function CEFOnSuccess(response) {
  switch (response.result) {
    case 'ok':
      CEFShowResult(CEFFormVars.successText, 'cef_success');
      CEFResetForm();
      break;

    case 'error':
      CEFShowErrors(response.fields);
      break;

    default:
      break;
  }
}

/**
 * Form submit callback
 *
 * @param {string} cefFormId
 * @returns void
 */
function CEFOnSubmit(cefFormId) {
  setActiveFormId(cefFormId);

  var validationResult = CEFValidateFields();
  var formData;

  if (validationResult.result === 'ok') {
    formData = validationResult.fields;
    formData.nonce = CEFFormVars.nonce;
    formData.action = 'cef_process_form_data';
    CEFSendData(CEFFormVars.ajaxUrl, formData, CEFOnSuccess, CEFShowResult);
    return;
  }

  CEFShowErrors(validationResult.fields);
}

/**
 * Attaches event handlers for every .cef_enquiry_form element.
 * (submit handlers for the forms and click handlers for the back buttons)
 *
 * @return void
 */
function CEFAttachEventHandlers() {
  $('.cef_enquiry_form').each(function () {
    var cefFormId = this.id;

    $('.cef_enquiry_form#' + cefFormId + ' .cef_form').submit(function (event) {
      event.preventDefault();
      CEFOnSubmit(cefFormId);
    });

    $('.cef_enquiry_form#' + cefFormId + ' .cef_back').click(function (event) {
      event.preventDefault();
      CEFHideResult(cefFormId);
    });
  });
}

$(document).ready(CEFAttachEventHandlers);
