=== Codeable Enquiry Form ===
A simple form for performing enquiries to the users of your site.

== Description ==
This plugin allows you to show a form in your website to perform enquiries.
You can show your form in any post or page using this shortcode: [cef_form].
You can also see the data that users has submitted in your form by using
the [cef_data_list] shortcode which shows a list of the entries in the
database. If you click in any entry of the list, you will see the all fields of that entry
below the list. (Note: the [cef_data_list] shortcode only works for admin users!)

== Installation ==
1. Go to the 'Plugins' page in the Wordpress Admin Page.
2. Click on 'Add New' (at the top left)
3. Click on 'Upload Plugin' (at the top left)
4. Click on 'Select File'
5. Select the plugin zip file
6. Click on 'Install Now'
7. And Finally Click on 'Activate Plugin'.
