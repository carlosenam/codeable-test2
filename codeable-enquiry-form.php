<?php
/**
 * Plugin Name: Codeable Enquiry Form
 * Description: A simple form for performing enquiries to the users of your site
 * Author:      Carlos Alvarez
 * Version:     1.0.0
 * Text Domain: cef
 *
 * @package CEF
 */

// Prevents direct access to this file.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

define( 'CEF_VERSION', get_file_data( __FILE__, array( 'Version' ) )[0] );
define( 'CEF_PATH', plugin_dir_path( __FILE__ ) );
define( 'CEF_INC_PATH', CEF_PATH . 'includes/' );
define( 'CEF_URL', plugin_dir_url( __FILE__ ) );
define( 'CEF_ASSETS_URL', CEF_URL . 'assets/' );
define( 'CEF_TABLE_NAME', 'cef_form_data' );


/**
 * Main class of the plugin
 */
class CEF_MainClass {

	/**
	 * Single class instance
	 *
	 * @var CEF_MainClass
	 */
	protected static $instance = null;

	/**
	 * Registers the activation hook and add an action to add the shortcodes
	 * in the 'init' hook
	 */
	private function __construct() {
		register_activation_hook( __FILE__, array( $this, 'activate' ) );
		add_action( 'init', array( $this, 'add_shortcodes' ) );
	}

	/**
	 * Add the shortcodes [cef_form] and [cef_data_list]
	 *
	 * @return void
	 */
	public function add_shortcodes() {
		include_once CEF_INC_PATH . 'models/enquiry-data.php';
		include_once CEF_INC_PATH . 'shortcodes/form.php';
		include_once CEF_INC_PATH . 'shortcodes/data-list.php';

		new CEF_Form_Shortcode();
		// Only admin users can see the data lists!
		if ( in_array( 'administrator', wp_get_current_user()->roles, true ) ) {
			new CEF_DataList_Shortcode();
		}
	}

	/**
	 * Create database table
	 *
	 * @return void
	 */
	public function activate() {
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();

		$table_name = $this->get_table_name();

		if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) !== $table_name ) {

			$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			  id INT NOT NULL AUTO_INCREMENT,
			  first_name VARCHAR(100),
			  last_name VARCHAR(100),
			  email VARCHAR(100),
			  subject VARCHAR(100),
			  message TEXT,
			  date DATETIME,
			  PRIMARY KEY  (id),
			  KEY email_key (email),
			  KEY date_key (date)) $charset_collate";

			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
			dbDelta( $sql );
		}
	}

	/**
	 * Returns the database table name
	 *
	 * @return string
	 */
	public function get_table_name() {
		global $wpdb;
		return $wpdb->prefix . CEF_TABLE_NAME;
	}

	/**
	 * An unique ID is generated for every form and data list.
	 * This is to avoid problems when two forms or two data lists are in
	 * the same page
	 *
	 * @return string An Unique ID
	 * @throws Exception When the functions 'random_bytes' and
	 * 'openssl_random_pseudo_bytes' are not defined.
	 */
	public function generate_unique_id() {
		$lenght = 13;
		if ( function_exists( 'random_bytes' ) ) {
			$bytes = random_bytes( ceil( $lenght / 2 ) );
		} elseif ( function_exists( 'openssl_random_pseudo_bytes' ) ) {
			$bytes = openssl_random_pseudo_bytes( ceil( $lenght / 2 ) );
		} else {
			throw new Exception( 'no cryptographically secure random function available' );
		}
		return 'cef_' . substr( bin2hex( $bytes ), 0, $lenght );
	}

	/**
	 * Ensures only one instance of CEF is created
	 *
	 * @return CEF_MainClass
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

}

/**
 * Returns the CEF_MainClass instance
 *
 * @return CEF_MainClass CEF instance
 */
function CEF() {
	return CEF_MainClass::instance();
}

CEF();
